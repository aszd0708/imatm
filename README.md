# I'm ATM

## 소개
ATM이 고장났다!? 일하러간 알바생이 일한곳은 ATM앞!!

제한시간 내에 사람들의 돈을 정확하게 인출해라!!

## 플레이 영상
[![youtubeLink](https://img.youtube.com/vi/jsvILxS8fJE/sddefault.jpg)](https://www.youtube.com/watch?v=jsvILxS8fJE)

## 다운로드
[![icon](https://play-lh.googleusercontent.com/26hpirf84UaYptKl3MJJ1oMcrY2Kz6saSeXkAIyeRHWp5iGtaOUjS8Yxo3x044YaFA=s180-rw)](https://play.google.com/store/apps/details?id=com.MatZip.ImATM)

[apk 다운로드](https://play.google.com/store/apps/details?id=com.MatZip.ImATM)

## 게임 방법
1. 플레이 버튼을 누르면 나오는 사람들의 돈을 우측 시간이 다 지나기 전에 인출 해주자!
2. 틀리게 인출하거나 제한시간에 인출을 못했을 경우 실해한다.
3. 각 지폐의 갯수가 모자르거나 남아있지 않으면 광고를 보고 충전하자!
4. 가끔씩 VIP가 와서 많이 인출해준다!!!

## 사용한 라이브러리
[DoTween](http://dotween.demigiant.com/) : 움직임을 간단하게 구현 할 때 사용

[GoogleGamePlayService](https://developers.google.com/games/services/integration) : 구글플레이와 연동

[Newtonsoft.Json](https://github.com/JamesNK/Newtonsoft.Json/releases) : Json 연동

## 직접 제작한 유틸리티
[UnityManagers](https://github.com/aszd0708/UnityGameManagers) : 저장, 풀링, 팝업등 사용

## 제작
### 기간
2020년 10월 ~ 2020년 12월
약 2달

### 인원
레벨 디자인 및 밸런스 조절 1명
프로그래밍 및 아이디어 1명

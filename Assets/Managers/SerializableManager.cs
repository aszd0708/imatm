﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

/*
 * 직렬화 해서 파일로 만들어 저장하는 함수
 * 제네릭 함수를 사용하여 원하는 자료형으로 만들 수 있음
 * 와! 개 편할듯!
 */

public class SerializableManager<T> : MonoBehaviour
{
    private string saveDataName;
    private string saveAddress;

    protected virtual string SaveDataName { get => saveDataName; set => saveDataName = value; }
    public string SaveAddress { get => saveAddress; set => saveAddress = value; }

    protected void SetFileName(string fileName)
    {
        SaveDataName = fileName;
        SaveAddress = Application.persistentDataPath + "/" + SaveDataName + ".dat";
    }

    // 데이터를 저장해서 파일로 만들어 생성
    protected void SaveData(List<T> saveData)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(SaveAddress);

        List<T> saveInfo = new List<T>();

        saveInfo = saveData;

        bf.Serialize(file, saveInfo);
        file.Close();
    }

    // 만든 이름을 찾아서 갖고옴
    protected List<T> LoadData()
    {
        List<T> saveInfo = new List<T>();

        if (!System.IO.File.Exists(SaveAddress))
        {
            return null;
        }
        else
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.OpenRead(SaveAddress);

            if (file != null && file.Length > 0)
            {
                // 파일 역직렬화하여 B에 담기
                saveInfo = (List<T>)bf.Deserialize(file);
                // B --> A에 할당
                file.Close();
            }
        }

        return saveInfo;
    }

    public void SaveData(T saveData)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(SaveAddress);

        T saveInfo = saveData;

        bf.Serialize(file, saveData);
        file.Close();
    }

    protected T LoadData<T>()
    {
        T saveInfo = default;

        if (!System.IO.File.Exists(SaveAddress))
        {
            return default;
        }
        else
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.OpenRead(SaveAddress);

            if (file != null && file.Length > 0)
            {
                // 파일 역직렬화하여 B에 담기
                saveInfo = (T)bf.Deserialize(file);
                // B --> A에 할당
                file.Close();
            }
        }

        return saveInfo;
    }

    protected void DeleteData()
    {
        System.IO.File.Delete(SaveAddress);
        //Debug.Log(SaveDataName + "삭제됨");
    }

    public byte[] GetDataToByte()
    {
        byte[] data = null;

        if (!System.IO.File.Exists(SaveAddress))
        {
            return default;
        }

        else
        {
            data = File.ReadAllBytes(SaveAddress);
        }

        return data;
    }

    public void SetDataToByte(byte[] data)
    {
        File.WriteAllBytes(SaveAddress, data);
    }
}
﻿using GoogleMobileAds.Api;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/*
 * 2020-06-02
 * 광고보고 생명 하나 더 주는 대충 그런 클래스
 */

public class AdMobManager : GoogleAdManager
{
    [SerializeField]
    public class AdInfo
    {
        private string androidAdCode;
        private string appleAdCode;

        public UnityEvent rewardEvent;

        public RewardedAd rewardedAd;

        public bool isShow = false;

        private AdRequest request;

        public AdInfo(string _androidAdCode, string _appleAdCode, UnityEvent _rewardEvent)
        {
            if (_androidAdCode == string.Empty)
                androidAdCode = null;
            else
                androidAdCode = _androidAdCode;
            if (_appleAdCode == string.Empty)
                appleAdCode = null;
            else
                appleAdCode = _appleAdCode;
            rewardEvent = _rewardEvent;

            RequestInterstitial();
        }

        public void RequestInterstitial()
        {
#if UNITY_ANDROID
            string adUnitId = androidAdCode;
#elif UNITY_IPHONE
        string adUnitId = appleAdCode;
#else
        string adUnitId = "unexpected_platform";
#endif

            rewardedAd = new RewardedAd(adUnitId);
            this.rewardedAd.OnAdLoaded += HandleRewardedAdLoaded;
            // Called when an ad request failed to load.
            this.rewardedAd.OnAdFailedToLoad += HandleRewardedAdFailedToLoad;
            // Called when an ad is shown.
            this.rewardedAd.OnAdOpening += HandleRewardedAdOpening;
            // Called when an ad request failed to show.
            this.rewardedAd.OnAdFailedToShow += HandleRewardedAdFailedToShow;
            // Called when the user should be rewarded for interacting with the ad.
            this.rewardedAd.OnUserEarnedReward += HandleUserEarnedReward;
            // Called when the ad is closed.
            this.rewardedAd.OnAdClosed += HandleRewardedAdClosed;

            AdRequest request = new AdRequest.Builder()
                .AddTestDevice(AdRequest.TestDeviceSimulator).AddTestDevice("92363741D9B756D79935C13DA2ADDDB0").
            Build();
            rewardedAd.LoadAd(request);
        }

        public void LoadAd()
        {
            rewardedAd.LoadAd(request);
        }

        public void HandleRewardedAdLoaded(object sender, EventArgs args)
        {
            //MonoBehaviour.print("HandleRewardedAdLoaded event received");
        }

        public void HandleRewardedAdFailedToLoad(object sender, AdErrorEventArgs args)
        {
            //MonoBehaviour.print(
            //    "HandleRewardedAdFailedToLoad event received with message: "
            //                     + args.Message);
        }

        public void HandleRewardedAdOpening(object sender, EventArgs args)
        {
            //MonoBehaviour.print("HandleRewardedAdOpening event received");
        }

        public void HandleRewardedAdFailedToShow(object sender, AdErrorEventArgs args)
        {
            //MonoBehaviour.print(
            //    "HandleRewardedAdFailedToShow event received with message: "
            //                     + args.Message);
        }

        public void HandleRewardedAdClosed(object sender, EventArgs args)
        {
            //MonoBehaviour.print("HandleRewardedAdClosed event received");
            //RequestInterstitial();
            LoadAd();
        }

        public void HandleUserEarnedReward(object sender, Reward args)
        {
            string type = args.Type;
            double amount = args.Amount;
            //MonoBehaviour.print(
            //    "HandleRewardedAdRewarded event received for "
            //                + amount.ToString() + " " + type);

            isShow = true;
            //RequestInterstitial();
        }

        public void HandleOnAdLoaded(object sender, EventArgs args)
        {
            //MonoBehaviour.print("HandleAdLoaded event received");
        }

        public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
        {
            //MonoBehaviour.print("광고 실패 이유 : "
            //                    + args.Message);
        }

        public void HandleOnAdOpened(object sender, EventArgs args)
        {
            //MonoBehaviour.print("HandleAdOpened event received");
        }

        public void HandleOnAdClosed(object sender, EventArgs args)
        {
            //MonoBehaviour.print("HandleAdClosed event received");
            // RequestInterstitial();
        }

        public void HandleOnAdLeavingApplication(object sender, EventArgs args)
        {
            //MonoBehaviour.print("HandleAdLeavingApplication event received");
        }

        //public void AdsShow()
        //{
        //    if (this.rewardedAd.IsLoaded())
        //    {
        //        this.rewardedAd.Show();
        //        print("광고 시작");
        //    }
        //    else
        //    {
        //        RequestInterstitial();
        //        print("광고 ss");
        //    }
        //}

        public void AdsShow()
        {
            if (this.rewardedAd.IsLoaded())
            {
                this.rewardedAd.Show();
                isShow = true;
            }
            else
            {
                RequestInterstitial();

                isShow = false;
            }
        }
    }

    public string[] androidAdCodes;
    public string[] appleAdCodes;

    public UnityEvent rewardEvent;

    // 처음 로드 쓰면 B로
    private Queue<AdInfo> adsA = new Queue<AdInfo>();
    // A에서 온거 다 차면 B 실행 후 A로 보냄
    private Queue<AdInfo> adsB = new Queue<AdInfo>();

    /// <summary>
    /// 큐 포인터 위에 있는 큐 지정해줌
    /// true 일때는 A false 일때는 B
    /// 이거 명심
    /// </summary>
    private bool queuePointer;

    private bool showAd;

    private void Start()
    {
        SetFirstQueue();
    }

    private AdInfo SetAdInfo(string _androidAdCode, string _appleAdCode)
    {
        AdInfo adInfo = new AdInfo(_androidAdCode, _appleAdCode, rewardEvent);
        //StartCoroutine(_CheckLoading(adInfo.rewardedAd));
        StartCoroutine(_GetReward(adInfo));
        return adInfo;
    }

    private void SetFirstQueue()
    {
        StartCoroutine(_SetFirstQueue());
    }

    private IEnumerator _SetFirstQueue()
    {
        WaitForSeconds wait = new WaitForSeconds(0.5f);
        for (int i = 0; i < androidAdCodes.Length; i++)
        {
            adsA.Enqueue(SetAdInfo(androidAdCodes[i], appleAdCodes[i]));
            yield return wait;
        }
        SetPointer();
        yield break;
    }

    private void SetPointer()
    {
        // A 일때 
        if (queuePointer)
        {
            if (adsA.Count <= 0)
                queuePointer = false;
            else return;
        }
        else
        {
            if (adsB.Count <= 0)
                queuePointer = true;
            else return;
        }
    }

    public void ShowAds()
    {
        AdInfo adInfo;
        if (queuePointer)
        {
            adInfo = adsA.Dequeue();
            adsB.Enqueue(adInfo);
        }

        else
        {
            adInfo = adsB.Dequeue();
            adsA.Enqueue(adInfo);
        }
        AdsShow(adInfo);
        SetPointer();
    }

    private IEnumerator _CheckLoading(RewardedAd rewarded)
    {
        WaitUntil wait = new WaitUntil(() => rewarded.IsLoaded());
        yield return wait;
        yield break;
    }

    private IEnumerator _GetReward(AdInfo adInfo)
    {
        WaitUntil wait = new WaitUntil(() => adInfo.isShow);
        while (true)
        {
            yield return wait;
            adInfo.isShow = false;

            adInfo.rewardEvent.Invoke();
        }
    }

    public void AdsShow(AdInfo adInfo)
    {
        if (adInfo.rewardedAd.IsLoaded())
        {
            adInfo.rewardedAd.Show();
        }
        else
        {
            adInfo.LoadAd();

            adInfo.isShow = false;
        }
    }
}
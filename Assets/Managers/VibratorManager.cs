﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VibratorManager : Singleton<VibratorManager>
{
    public bool vib;

    private void Start()
    {
        VibSetting();
    }

    private void VibSetting()
    {
        if (!PlayerPrefs.HasKey("Vibrator"))
            PlayerPrefs.SetInt("Vibrator", 1);

        if (PlayerPrefs.GetInt("Vibrator") == 0)
            vib = false;
        else if (PlayerPrefs.GetInt("Vibrator") == 1)
            vib = true;
        //else
        //    Debug.Log("진동 에러에러 빼애애애애액");
    }

    private void SetVibe(int isSet)
    {
        PlayerPrefs.SetInt("Vibrator", isSet);

        if (isSet == 0)
            vib = false;
        else vib = true;
    }

    public void SetVibe()
    {
        if (PlayerPrefs.GetInt("Vibrator") == 0)
        {
            PlayerPrefs.SetInt("Vibrator", 1);
            vib = true;
        }

        else if (PlayerPrefs.GetInt("Vibrator") == 1)
        {
            PlayerPrefs.SetInt("Vibrator", 0);
            vib = false;
        }

        //else Debug.Log("에러에러");
    }

    public void TouchVib()
    {
        //Debug.Log("터치");
        // 배경 터치했을때
        if (vib)
            Vibrator.CreateOneShot(100);
        else
            return;
    }

    public void WrongMoney()
    {
        if(vib)
            Vibrator.CreateOneShot(300);
        else
            return;
    }

    public void ObjTouchVib()
    {
       // Debug.Log("오브젝트 터치 진동 읭읭");
        // 
        if (vib)
            Vibrator.CreateOneShot(40);
        else return;
    }

    public void WrongObjTouch()
    {
        //Debug.Log("아이템 잘못 두는 진동 읭읭");
        // 아이템을 뒀을때 틀렸을때
        if (vib)
            Vibrator.CreateOneShot(200);
        else
            return;
    }
    public void CatRoomVib()
    {
        // 고양이집 진동인데 On/Off 기능 넣을까 뺄까??
       // Debug.Log("고양이집 진동 읭읭");

        if (vib)
            Vibrator.CreateOneShot(50);
        else
            return;

        //Vibrator.Vibrate(50);
    }
}

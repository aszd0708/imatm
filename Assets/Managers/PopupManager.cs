﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2020-06-02
 * 팝업을 컨트롤 해주는 클래스
 * 나오게 하거나 없어지게 해주고
 * 팝업이 나오는 타이밍에 따라서 터치를 제한해주는 기능이 있음
 */

public class PopupManager : Singleton<PopupManager>
{
    [Header("메뉴에서 나오는 팝업 카운트")]
    private int waitingPopupCount = 0;

    public int WaitingPopupCount
    {
        get => waitingPopupCount;
        set
        {
            waitingPopupCount = value;

            if (WaitingPopupCount < 0)
                WaitingPopupCount = 0;

            if (WaitingPopupCount > 0)
                for (int i = 0; i < waitingPopupNotTouch.Length; i++)
                    waitingPopupNotTouch[i].SetActive(false);
            else
                for (int i = 0; i < waitingPopupNotTouch.Length; i++)
                    waitingPopupNotTouch[i].SetActive(true);
        }
    }

    [Header("게임중 나오는 팝업 카운트")]
    private int gamingPopupCount = 0;

    public int GamingPopupCount
    {
        get => gamingPopupCount;
        set
        {
            if (value < 0)
                value = 0;

            if (value > 0)
            {
                for (int i = 0; i < gamingPopupNotTouch.Length; i++)
                    gamingPopupNotTouch[i].SetActive(false);
                timeManager.Pause(true);
            }

            else
            {
                for (int i = 0; i < gamingPopupNotTouch.Length; i++)
                    gamingPopupNotTouch[i].SetActive(true);
                //timeManager.Pause(false);
            }

            gamingPopupCount = value;
        }
    }

    [Header("메뉴 팝업 나올때 터치 못해야 하는 버튼")]
    public GameObject[] waitingPopupNotTouch;

    [Header("게임중 팝업나올 시 터치 못해야 하는 버튼")]
    public GameObject[] gamingPopupNotTouch;

    [Header("게임중 팝업 나올 시 시간컨트롤 할 클래스")]
    public TimeManager timeManager;

    [Header("팝업들")]
    public RectTransform[] popups;

    [Header("팝업 나오는 시간")]
    public float popupTime;

    private Vector3 popup01, popup10, popup11, popup00;
    private WaitForSeconds wait;

    public RectTransform exitRect;

    protected override void Awake()
    {
        base.Awake();

        popup01 = new Vector3(0.025f, 1);
        popup10 = new Vector3(1, 0.025f);
        popup11 = new Vector3(1, 1);
        wait = new WaitForSeconds(popupTime);

        for (int i = 0; i < popups.Length; i++)
        {
            popups[i].localScale = Vector3.zero;
            popups[i].gameObject.SetActive(false);
        }
    }

    private void Update()
    {
        bool isGameOver = GameManager.Instance.IsGameOver;
        if (Application.platform == RuntimePlatform.Android)
        {
            if (Input.GetKeyDown(KeyCode.Escape) && isGameOver)
            {
                OpenNotGamingPopup(exitRect);
            }
        }
    }

    public void OpenPopup(ref RectTransform popup, bool gaming)
    {
        //popup.DOScale(Vector3.one, popupTime).SetEase(Ease.Linear);
        StartCoroutine(OpenPopupAnimation(popup));

        if (gaming) GamingPopupCount++;
        else WaitingPopupCount++;

        if (popup.GetComponent<IMenuPanel>() != null)
            popup.GetComponent<IMenuPanel>().DoOn();
    }

    public void ClosedPopup(ref RectTransform popup, bool gaming)
    {
        //popup.DOScale(Vector3.zero, popupTime).SetEase(Ease.Linear);
        StartCoroutine(ClosePopupAnimation(popup));

        AudioManager.Instance.PlaySound(AudioManager.AudioKinds.WRONG, Vector2.zero);

        if (gaming) GamingPopupCount--;
        else WaitingPopupCount--;

        if (popup.GetComponent<IMenuPanel>() != null)
            popup.GetComponent<IMenuPanel>().DoOff();
    }

    public void OpenPopup(RectTransform popup, bool gaming)
    {
        //popup.DOScale(Vector3.one, popupTime).SetEase(Ease.Linear);
        StartCoroutine(OpenPopupAnimation(popup));

        if (gaming) GamingPopupCount++;
        else WaitingPopupCount++;

        if (popup.GetComponent<IMenuPanel>() != null)
            popup.GetComponent<IMenuPanel>().DoOn();
    }

    public void ClosedPopup(RectTransform popup, bool gaming)
    {
        //popup.DOScale(Vector3.zero, popupTime).SetEase(Ease.Linear);
        StartCoroutine(ClosePopupAnimation(popup));

        if (gaming) GamingPopupCount--;
        else WaitingPopupCount--;

        if (popup.GetComponent<IMenuPanel>() != null)
            popup.GetComponent<IMenuPanel>().DoOff();
    }

    public void ClosedPopup(RectTransform popup)
    {
        popup.DOScale(Vector3.zero, popupTime).SetEase(Ease.Linear);
    }

    public void OpenGamingPopup(RectTransform popup)
    {
        OpenPopup(popup, true);
    }

    public void OpenNotGamingPopup(RectTransform popup)
    {
        OpenPopup(popup, false);
    }

    public void CloseGamingPopup(RectTransform popup)
    {
        ClosedPopup(popup, true);
    }

    public void CloseNotGamingPopup(RectTransform popup)
    {
        ClosedPopup(popup, false);
    }

    private IEnumerator OpenPopupAnimation(RectTransform popup)
    {
        popup.gameObject.SetActive(true);
        popup.DOScale(popup01, popupTime).SetEase(Ease.OutExpo);
        yield return wait;
        popup.DOScale(Vector3.one, popupTime).SetEase(Ease.OutExpo);
        yield break;
    }

    private IEnumerator ClosePopupAnimation(RectTransform popup)
    {
        popup.DOScale(popup10, popupTime).SetEase(Ease.OutExpo);
        yield return wait;
        popup.DOScale(Vector3.zero, popupTime).SetEase(Ease.OutExpo);
        yield return wait;
        popup.gameObject.SetActive(false);
        yield break;
    }
}
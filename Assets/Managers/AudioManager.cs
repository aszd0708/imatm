﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//사운드이펙트 / 배경 음악 등 모든 오디오 관련 작업을 처리하는 클래스
public class AudioManager : Singleton<AudioManager>, IPoolingObject
{
    public AudioClip[] clips;       //모든 AudioClip을 저장할 배열
    public AudioSource audioPrefab; //AudioSource를 가지고 있는 프리팝

    public enum AudioKinds
    {
        COIN, BTN, PERSON, START, OVER, PAY, CLOSED, WRONG, VIPSELECT, LESSTIME, PARTYBTN, FEVER, EMPTYMONEY
    }

    [Header("각 사운드 오디오 클립")]
    public AudioClip[] coinClips;

    public AudioClip[] btnClips, personClips, startClips, overClips, payClips, closedClips, wrongClips, vipSelectClips,
    timeLessClips, partyBtnClips, feverClips, emptyMoneyClips;

    private string poolingName;
    public string PoolingName { get => poolingName; set => poolingName = value; }

    private void Start()
    {
        PoolingName = "SoundPrefeb";
    }

    // 값을 받아서 원하는 클립을 반환해주는 함수
    public AudioClip[] GetAudioClipsKinds(AudioKinds kinds)
    {
        switch (kinds)
        {
            case AudioKinds.COIN: return coinClips;
            case AudioKinds.BTN: return btnClips;
            case AudioKinds.PERSON: return personClips;
            case AudioKinds.START: return startClips;
            case AudioKinds.OVER: return overClips;
            case AudioKinds.PAY: return payClips;
            case AudioKinds.CLOSED: return closedClips;
            case AudioKinds.WRONG: return wrongClips;
            case AudioKinds.VIPSELECT: return vipSelectClips;
            case AudioKinds.PARTYBTN: return partyBtnClips;
            case AudioKinds.FEVER: return feverClips;
            case AudioKinds.LESSTIME: return timeLessClips;
            case AudioKinds.EMPTYMONEY: return emptyMoneyClips;
            default: return null;
        }
    }

    //이름으로 플레이할 사운드를 찾아서 재생해주는 함수 ( + 어떻게 재생할지 패러미터 추가)
    public void PlaySound(string clipName, Vector3 pos, Transform parent = null, float pitch = 1f)
    {
        //clips에 있는 모든 AudioClip을 순환하며, 전달받은 clipName과 같은 이름의 클립을 찾아줌
        foreach (AudioClip ac in clips)
            if (ac.name == clipName)
            {
                //찾은 소리를 실제로 재생해달라고 패러미터 넘겨줌
                PlaySound(ac, pos, parent, pitch);
                break;
            }
    }

    public void PlaySound(AudioClip[] clip, Vector3 pos, Transform parent = null, float pitch = 1f)
    {
        int randomIndex = Random.Range(0, clip.Length);
        PlaySound(clip[randomIndex], pos, parent, pitch);
    }

    public void PlaySound(AudioKinds kinds, Vector3 pos, Transform parent = null, float pitch = 1f)
    {
        PlaySound(GetAudioClipsKinds(kinds), pos, parent, pitch);
    }

    //여러 사운드 중 랜덤하게 골라 재생하는 PlaySound 오버로딩
    public void PlaySound(string[] clipNames, Vector3 pos, Transform parent = null, float pitch = 1f)
    {
        int randomIndex = Random.Range(0, clipNames.Length);        //랜덤한 인덱스 값 저장

        PlaySound(clipNames[randomIndex], pos, parent, pitch);      //선택된 오디오클립 플레이
    }

    //실제 오디오클립 재생하는 함수
    public void PlaySound(AudioClip audioClip, Vector3 pos, Transform parent = null, float pitch = 1f)
    {
        //오디오 재생용 프리팝 생성
        GameObject audioObj = PoolingManager.Instance.GetPool(PoolingName);

        if (audioObj == null) audioObj = Instantiate<GameObject>(audioPrefab.gameObject, pos, Quaternion.identity);

        AudioSource audioInstance = audioObj.GetComponent<AudioSource>();

        //소리를 내는 사물을 따라가야 하는 경우, 그 사물의 자식으로 넣어줌
        if (parent != null)
            audioInstance.transform.SetParent(parent);

        audioInstance.clip = audioClip;     //클립을 바꿔주고
        audioInstance.pitch = pitch;        //피치값 설정
        audioInstance.volume = PlayerPrefs.GetInt("EffectSound", 1);
        audioInstance.Play();               //재생

        PoolingManager.Instance.SetPool(audioObj, PoolingName, audioClip.length);

        //Debug.Log("재생됨 : " + audioClip.name);
    }

    public AudioSource GetPlaySound(AudioClip audioClip, Vector3 pos, Transform parent = null, float pitch = 1f)
    {
        //오디오 재생용 프리팝 생성
        AudioSource audioInstance = Instantiate(audioPrefab, pos, Quaternion.identity);

        //소리를 내는 사물을 따라가야 하는 경우, 그 사물의 자식으로 넣어줌
        if (parent != null)
            audioInstance.transform.SetParent(parent);

        audioInstance.clip = audioClip;     //클립을 바꿔주고
        audioInstance.pitch = pitch;        //피치값 설정
        audioInstance.volume = PlayerPrefs.GetInt("EffectSound", 1);
        audioInstance.Play();               //재생

        return audioInstance;
    }

    public AudioSource GetPlaySound(AudioClip[] clip, Vector3 pos, Transform parent = null, float pitch = 1f)
    {
        int randomIndex = Random.Range(0, clip.Length);
        return GetPlaySound(clip[randomIndex], pos, parent, pitch);
    }

    public AudioSource GetPlaySound(AudioKinds kinds, Vector3 pos, Transform parent = null, float pitch = 1f)
    {
        return GetPlaySound(GetAudioClipsKinds(kinds), pos, parent, pitch);
    }
}
﻿using GoogleMobileAds.Api;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoogleAdManager : MonoBehaviour
{
    [Header("App ID")]
    public string androidAppID;
    public string iphoneAppID;

    [Header("AdUnitID")]
    public string androidAdUnitID;
    public string iphoneAdUnitID;

    protected BannerView bannerView;

    protected RewardedAd rewardedAd;

    [Obsolete]
    private void Awake()
    {
#if UNITY_ANDROID
        string appId = androidAppID;
#elif UNITY_IPHONE
        string appId = iphoneAppID;
#else
        string appId = "unexpected_platform";
#endif
        
        MobileAds.Initialize(appId);
    }
}

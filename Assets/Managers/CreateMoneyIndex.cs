﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MoneyInfo
{
    public int touchCount;
    public List<int> money = new List<int>();
}

public class CreateMoneyIndex : SerializableManager<MoneyInfo>
{
    [Header("최대 최소 가격")]
    public int minMoney;
    public int maxMoney;
    
    public MoneyInfo[] info = new MoneyInfo[21];

    public int[] money;

    private void Start()
    {
        SetFileName("Data");
        Cal();
    }

    private int GreedyAl(int value)
    {
        int count = 0;

        for (int i = 0; i < money.Length; i++)
        {
            if (money[i] > value)
                i++;
            else if (money[i] <= value)
            {
                count += value / money[i];
                value %= money[i];
                count++;
            }
            else
            {
                break;
            }
        }
        return count;
    }

    private void Cal()
    {
        for (int index = 10; index <= 750000; index += 10)
        {
            int count = GreedyAl(index);

            if (count >= 20)
            {
                Debug.Log(index);
                continue;
            }

            else
            {
                info[count].money.Add(index);
                info[count].touchCount = count;
            }

        }

        List<MoneyInfo> infoList = new List<MoneyInfo>();
        for (int i = 0; i < info.Length; i++)
            infoList.Add(info[i]);

        SaveData(infoList);
    }
}

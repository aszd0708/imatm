﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2020-05-28
 * 웨이블 레벨 값들을 전부 제이슨으로 받아와서 분리 해주는 스크립트
 */

[System.Serializable]
public class WaveLevelInfo
{
    public string WaveLevel;
    public string PersonCount;
    public string TouchCount;
    public string Percent;
    public string Score;

    private List<int> personCountList = new List<int>();
    private List<int> touchCountList = new List<int>();
    private List<int> percentList = new List<int>();

    private int scoreInt;

    public List<int> PersonCountList { get => personCountList; set => personCountList = value; }
    public List<int> TouchCountList { get => touchCountList; set => touchCountList = value; }
    public List<int> PercentList { get => percentList; set => percentList = value; }
    public int ScoreInt 
    {
        get
        {
            return System.Convert.ToInt32(Score);
        }

        set => scoreInt = value; 
    }

    /// <summary>
    /// 받은 값에서 , 이걸 기준으로 분리해서 리스트 반환하는 함수
    /// </summary>
    /// <param 제이슨 문자열="counts"></param>
    /// <returns></returns>
    public List<int> SettingCountList(string counts)
    {
        int first = 0;

        List<int> values = new List<int>();
        char[] temp = counts.ToCharArray();
        for (int i = 0; i < temp.Length; i++)
        {
            int last;

            if (temp[i] == '_')
            {
                last = i - 1;
                char[] sendValue = new char[last - first + 1];
                for (int a = first, index = 0; a <= last; a++, index++)
                    sendValue[index] = temp[a];
                values.Add(StringToInt(sendValue));
                first = i + 1;
            }

            if (i == temp.Length - 1)
            {
                last = i;
                char[] sendValue = new char[last - first + 1];
                for (int a = first, index = 0; a <= last; a++, index++)
                    sendValue[index] = temp[a];
                values.Add(StringToInt(sendValue));
            }
        }

        return values;
    }

    /// <summary>
    /// 받은 문자들을 int형으로 변환해주는 함수
    /// </summary>
    /// <param 받은 문자배열="numbers"></param>
    /// <returns></returns>
    private int StringToInt(char[] numbers)
    {
        string num = string.Concat(numbers);
        int number = Convert.ToInt32(num);
        return number;
    }
}

public class WaveLevelSettingManager : JsonDataLoadManager<WaveLevelInfo>
{
    [Header("제이슨 파일(웨이브 레벨)")]
    public TextAsset jsonFile;

    [SerializeField]
    private List<WaveLevelInfo> waveLevelInfos = new List<WaveLevelInfo>();

    public List<WaveLevelInfo> WaveLevelInfos { get => waveLevelInfos; set => waveLevelInfos = value; }

    protected void SetInfos()
    {
        WaveLevelInfos = LoadJson(jsonFile);
        for (int i = 0; i < WaveLevelInfos.Count; i++)
        {
            WaveLevelInfos[i].PersonCountList = WaveLevelInfos[i].SettingCountList(WaveLevelInfos[i].PersonCount);
            WaveLevelInfos[i].TouchCountList = WaveLevelInfos[i].SettingCountList(WaveLevelInfos[i].TouchCount);
            WaveLevelInfos[i].PercentList = WaveLevelInfos[i].SettingCountList(WaveLevelInfos[i].Percent);
        }
    }
}

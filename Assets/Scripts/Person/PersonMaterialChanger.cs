﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonMaterialChanger : MonoBehaviour
{
    public SpriteRenderer[] changeSprites;

    public Material originMaterial;

    public SpriteRenderer moneyTextEffectSprite;

    //public void OnEnable()
    //{
    //    MaterialChange(originMaterial);
    //}

    public void OnDisable()
    {
        MaterialChange(originMaterial);
        if (moneyTextEffectSprite != null)
            moneyTextEffectSprite.enabled = false;
    }

    public void MaterialChange(Material changeMaterial)
    {
        for (int i = 0; i < changeSprites.Length; i++)
            changeSprites[i].material = changeMaterial;
    }
}

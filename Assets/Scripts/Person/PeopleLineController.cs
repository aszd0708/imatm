﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeopleLineController : MonoBehaviour
{
    [SerializeField]
    private Queue<GameObject> peopleQ = new Queue<GameObject>();
    public Queue<GameObject> PeopleQ { get => peopleQ; set => peopleQ = value; }

    private PeopleCreater creater;
    private PeopleMoveController moveController;

    //private WaitForSeconds wait;

    private void Awake()
    {
        creater = GetComponent<PeopleCreater>();
        moveController = GetComponent<PeopleMoveController>();
    }

    private void Start()
    {
        //wait = new WaitForSeconds(moveController.moveTime * 20);

    }

    private void StartSetting()
    {
        PeopleQ.Clear();
        creater.SendMessage("SetCreateFirstLine", SendMessageOptions.DontRequireReceiver);
    }

    private void GameOverSetting()
    {
        creater.SendMessage("AllDeleteQueue", SendMessageOptions.DontRequireReceiver);
    }

    private void Answer(bool answer)
    {
        if(answer)
        {
            StartCoroutine(_Answer());
        }

        else
        {
            // 오답
        }
    }

    private IEnumerator _Answer()
    {
        creater.SendMessage("DeleteQueue", SendMessageOptions.DontRequireReceiver);
        creater.CreatePerson();
        moveController.SendMessage("MovePeople", SendMessageOptions.DontRequireReceiver);
       // yield return wait;
        yield break;
    }
}

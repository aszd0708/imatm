﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class PeopleMoveController : MonoBehaviour
{
    [Header("움직이는 시간")]
    public float moveTime;
    private PeopleCreater creater;
    
    [Header("줄 대각선 간격")]
    public float diagonalGap;

    [Header("첫번째 사람과 그 다음사람 간격")]
    public float firstPoersonGap;

    [Header("내는 버튼 금지하려고 넣음")]
    public Button btn;

    private List<Vector3> linePosesList = new List<Vector3>();

    private WaitForSeconds wait;


    private void Awake()
    {
        creater = GetComponent<PeopleCreater>();
    }

    private void Start()
    {
        wait = new WaitForSeconds(moveTime / 2);
    }

    private void MovePeople()
    {
        StartCoroutine(_MovePeople());
    }

    private IEnumerator _MovePeople()
    {
        //btn.enabled = false;
        for (int i = 0; i < transform.childCount; i++)
        {
            PersonMove personMove = transform.GetChild(i).GetComponent<PersonMove>();
            personMove.MoveToPose(linePosesList[i], moveTime);
            if(i == 0)
            {
                if (personMove.CompareTag("NormalPerson"))
                    personMove.GetComponent<PersonMaterialChanger>().moneyTextEffectSprite.enabled = true;
            }
            yield return wait;
        }
        CheckLine();
        //btn.enabled = true;
        yield break;
    }

    private void SetLineList()
    {
        linePosesList.Add(new Vector3(transform.GetChild(0).position.x , firstPoersonGap));
        for(int i = 1; i < transform.childCount; i++)
            linePosesList.Add(new Vector3(transform.GetChild(0).position.x + (diagonalGap * i), creater.gap * (i)));
    }

    private void CheckLine()
    {
        for(int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).position = linePosesList[i];
        }
    }
}

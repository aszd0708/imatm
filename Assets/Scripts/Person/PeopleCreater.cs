﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeopleCreater : MonoBehaviour
{
    [Header("사람들이 최대 줄 서는 수")]
    public int maxCount;

    [Header("기준이 되는 포지션")]
    public Vector3 firstCreatePos;

    [Header("줄 서는데 각 사람마다의 간격")]
    public float gap;

    [Header("줄 대각선 간격")]
    public float diagonalGap;
    [Header("처음 사람과 간격")]
    public float firstPoersonGap;

    [Header("사람들 Prefab")]
    public GameObject[] person;

    [Header("각 사람들 나올 확률")]
    public float[] createPercent;

    [Header("풀링 이름 도적히 따로 못뺄거 같아서 만듦 생각나면 다시 ㄱㄱ")]
    public string[] poolingName;

    [Header("생기는 카운트 재기 위한 웨이브 컨트롤러")]
    public WaveLevelController waveLevel;

    [Header("VIP 사람 만드는 깐쮸롤러")]
    public VIPPersonController vipController;
    [Header("어떤 VIP님이 오실지 데이터")]
    public UserDataSaveLoad userData;

    [Header("스코어")]
    public ScoreManager score;

    [Header("VIP 인덱스")]
    private int vipIndex;

    private bool canICreateVIP;
    public bool CanICreateVIP { get => canICreateVIP; set => canICreateVIP = value; }
    public int VipIndex
    {
        get
        {
            if (vipIndex >= vipController.info.PeopleInfo.Count)
            {
                vipIndex = vipController.info.PeopleInfo.Count;
            }
            return vipIndex;
        }

        set => vipIndex = value;
    }


    private PeopleLineController controller;
    private PeopleMoveController moveController;


    private void Awake()
    {
        controller = GetComponent<PeopleLineController>();
        moveController = GetComponent<PeopleMoveController>();
    }

    private void SetCreateFirstLine()
    {
        VipIndex = userData.UserData.VipPersonCollect;
        StartCoroutine(_SetCreateFirstLine());
    }

    private IEnumerator _SetCreateFirstLine()
    {
        WaitForSeconds waitSecond = new WaitForSeconds(0.1f);
        bool bonus = false;

        int countIndex = 0;
        if (userData.UserData.SelectFirstVipPerson != -1)
        {
            countIndex = 1;
            bonus = true;
        }

        if (bonus)
        {
            GameObject createPerson = vipController.GetFirstVIPPerson();
            createPerson.transform.SetParent(transform);
            controller.PeopleQ.Enqueue(createPerson);
            createPerson.transform.position = new Vector3(firstCreatePos.x, firstCreatePos.y + gap + firstPoersonGap);
            waveLevel.PeopleCount++;
            CanICreateVIP = false;
            yield return waitSecond;
        }

        else
        {
            CanICreateVIP = true;
        }

        for (int i = countIndex; i < maxCount; i++)
        {
            GameObject createPerson = Instantiate(RandomValue.GetRandomValue(person, createPercent));
            createPerson.transform.SetParent(transform);
            controller.PeopleQ.Enqueue(createPerson);
            if (i != 0)
                firstPoersonGap = 0;
            createPerson.transform.position = new Vector3(firstCreatePos.x + (diagonalGap * i), firstCreatePos.y + gap * (i + 1) + firstPoersonGap);
            waveLevel.PeopleCount++;
            if (i == 0)
                if (createPerson.CompareTag("NormalPerson"))
                    createPerson.GetComponent<PersonMaterialChanger>().moneyTextEffectSprite.enabled = true;
            yield return waitSecond;
        }

        controller.PeopleQ.Peek().SendMessage("SendPersonInfo", SendMessageOptions.DontRequireReceiver);
        yield break;
    }

    public void CreatePerson()
    {
        GameObject createPerson;

        //Debug.Log("CreatePerson VIP Index : " + VipIndex);

        if (vipController.CanGetVipPerson(VipIndex) && CanICreateVIP)
        {
            createPerson = PoolingManager.Instance.GetPool("VIP");
            if (createPerson == null)
                createPerson = vipController.GetVipPerson();
            else
                vipController.SetVipPerson(createPerson);
            VipIndex++;
            CanICreateVIP = false;
            //createPerson.GetComponent<>
        }

        else
        {
            int randomValue = 0;
            createPerson = PoolingManager.Instance.GetPool(poolingName[randomValue]);
            if (createPerson == null)
                createPerson = Instantiate(person[randomValue]);
        }

        createPerson.transform.SetParent(transform);
        createPerson.transform.position = new Vector3(0, gap * (maxCount - 1));
        //Debug.Log(new Vector3(0, gap * (maxCount - 1)));
        controller.PeopleQ.Enqueue(createPerson);
        waveLevel.PeopleCount++;
        moveController.SendMessage("SetLineList", SendMessageOptions.DontRequireReceiver);
    }

    private void DeleteQueue()
    {
        PoolingManager.Instance.SetPool(controller.PeopleQ.Peek(), controller.PeopleQ.Dequeue().GetComponent<IPoolingObject>().PoolingName);
        //controller.PeopleQ.Peek();
        // 순서때매 위에걸로 바꿈
        controller.PeopleQ.Peek().SendMessage("SendPersonInfo", SendMessageOptions.DontRequireReceiver);
    }

    private void AllDeleteQueue()
    {
        int countQ = controller.PeopleQ.Count;
        for (int i = 0; i < countQ; i++)
            PoolingManager.Instance.SetPool(controller.PeopleQ.Peek(), controller.PeopleQ.Dequeue().GetComponent<IPoolingObject>().PoolingName);
        controller.PeopleQ.Clear();
    }
}

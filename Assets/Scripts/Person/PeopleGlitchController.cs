﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeopleGlitchController : MonoBehaviour
{
    [Header("글리치 매테리얼")]
    public Material glitchMat;
    [Header("안 글리치 매테리얼")]
    public Material noneGlitchMat;

    [Header("글리치 타임")]
    public float glitchMinTime, glitchMaxTime;

    [Header("글리치 쉬는 타임")]
    public float restMinTime, restMaxTime;

    public GameObject[] personPrefabs;

    public Transform peopleLine;

    private Coroutine glitchCor;

    private void OnEnable()
    {
        //GlitchStart();
    }

    public void SetNormalMat()
    {
        //glitchCor = StartCoroutine(_GlichStart());
        for (int i = 0; i < peopleLine.childCount; i++)
            peopleLine.GetChild(i).GetComponent<PersonMaterialChanger>().MaterialChange(noneGlitchMat);
        for (int i = 0; i < personPrefabs.Length; i++)
            personPrefabs[i].GetComponent<PersonMaterialChanger>().MaterialChange(noneGlitchMat);
    }

    private IEnumerator _GlichStart()
    {
        while(true)
        {
            //glitchMat.DisableKeyword("GLITCH_ON");
            float stopTime = Random.Range(restMinTime, restMaxTime);
            for (int i = 0; i < peopleLine.childCount; i++)
                peopleLine.GetChild(i).GetComponent<PersonMaterialChanger>().MaterialChange(noneGlitchMat);
            for (int i = 0; i < personPrefabs.Length; i++)
                personPrefabs[i].GetComponent<PersonMaterialChanger>().MaterialChange(noneGlitchMat);
            yield return new WaitForSeconds(stopTime);

            stopTime = Random.Range(glitchMinTime, glitchMaxTime);
            //glitchMat.EnableKeyword("GLITCH_ON");
            for (int i = 0; i < peopleLine.childCount; i++)
                peopleLine.GetChild(i).GetComponent<PersonMaterialChanger>().MaterialChange(glitchMat);
            for (int i = 0; i < personPrefabs.Length; i++)
                personPrefabs[i].GetComponent<PersonMaterialChanger>().MaterialChange(glitchMat);
            yield return new WaitForSeconds(stopTime);
        }
    }

    public void StopGlitch()
    {
        //StopCoroutine(glitchCor);
    }
}

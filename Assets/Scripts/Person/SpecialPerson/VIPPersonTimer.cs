﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VIPPersonTimer : MonoBehaviour
{
    [Header("타이머UI")]
    public Image timer;

    [Header("타이머 Text")]
    public Text timerText;

    [Header("최대 시간")]
    public float maxTime;

    [Header("줄이 앞당겨지는 시간 체크하기 위해 넣어둠")]
    public PeopleMoveController move;

    private float nowTime;
    public float NowTime { get => nowTime; set => nowTime = value; }

    public AudioSource bgmAudio;
    public AudioSource feverAudio;

    private float plusTime;

    private Coroutine timerCor;

    private VIPPersonController controller;

    private void Awake()
    {
        controller = FindObjectOfType<VIPPersonController>();
    }

    private void Start()
    {
        NowTime = maxTime;
    }

    public void TimerStart()
    {
        NowTime = maxTime;
        timerCor = StartCoroutine(_Timer());
    }

    private IEnumerator _Timer()
    {
        bgmAudio.mute = true;
        feverAudio.Play();
        while (NowTime >= 0)
        {
            NowTime -= Time.deltaTime;
            timer.fillAmount = NowTime / maxTime;
            timer.color = new Color(1, timer.fillAmount, timer.fillAmount);
            timerText.text = string.Format("{0:f2}", NowTime);
            yield return null;
        }
        feverAudio.Stop();
        timerText.text = string.Format("{0:f2}", 7);
        controller.BonusStop();
        bgmAudio.mute = false;
        yield break;
    }
}
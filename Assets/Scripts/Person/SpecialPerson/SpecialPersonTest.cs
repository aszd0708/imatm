﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialPersonTest : MonoBehaviour, IPoolingObject
{
    private VIPPersonController controller;

    private string poolingName;

    public string PoolingName { get => poolingName; set => poolingName = value; }

    public string poolingObjName;

    private void Awake()
    {
        controller = FindObjectOfType<VIPPersonController>();
    }

    private void Start()
    {
        PoolingName = poolingObjName;
    }

    public void SendPersonInfo()
    {
        Debug.Log("asd");
        
        controller.BonusStart(1000);
    }
}

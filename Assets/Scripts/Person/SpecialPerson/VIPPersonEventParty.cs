﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class VIPPersonEventParty : MonoBehaviour
{
    [Header("미러볼 RectTransform")]
    public RectTransform mirroballRect;

    [Header("미러볼 내려올라가는 시간")]
    public float mirroballMoveTime;

    [Header("모든 스프라이트 끌래스")]
    public Image mirroballImg;

    [Header("불빛들이 바뀌는 시간")]
    public float lightChangeTime;

    private WaitForSeconds lightChangeWait;

    private Color changeColor;

    [Header("총 모션 타임")]
    public float motionTime;

    private WaitForSeconds motionWait;

    [Header("파티 불빛 클래스")]
    public VIPPersonPartyLighting lightingAnime;

    [Header("파티 파티클")]
    public GameObject partyParticle;

    [Header("파티클 풀링 이름")]
    public string particlePoolingName;

    [Header("글래치 효과")]
    public PeopleGlitchController glitchController;

    [Header("사람 라인")]
    public Transform peopleLine;

    [Header("피버 매테리얼")]
    public Material feverMat;

    [Header("배경 컴포넌트")]
    public GameObject normalBGImage, feverBGImage;

    [Header("몇번 터치했을때 한번 나오는지")]
    public int particleMaxTouchCount = 3;
    private int particleTouchCount = 0;

    private void Start()
    {
        lightChangeWait = new WaitForSeconds(lightChangeTime);
        motionWait = new WaitForSeconds(motionTime);
    }

    public void FeverStart()
    {
        //glitchController.StopGlitch();
        //peopleLine.GetChild(0).GetComponent<PersonMaterialChanger>

        feverBGImage.SetActive(true);
        normalBGImage.SetActive(false);

        for (int i = 0; i < peopleLine.childCount; i++)
        {
            PersonMaterialChanger matChager = peopleLine.GetChild(i).GetComponent<PersonMaterialChanger>();
            matChager.MaterialChange(feverMat);
        }
        StartCoroutine(_FeverStart());
    }

    private IEnumerator _FeverStart()
    {
        yield return motionWait;
        FeverEnd();
        yield break;
    }

    private void FeverEnd()
    {
        feverBGImage.SetActive(false);
        normalBGImage.SetActive(true);
        glitchController.SetNormalMat();
    }

    private IEnumerator _PartyMotion()
    {
        lightingAnime.Lighting();
        for (float i = 0; i < motionTime; i += lightChangeTime)
        {
            mirroballImg.transform.rotation = Quaternion.Euler(0, i * 180, 0);
            yield return lightChangeWait;
        }
        lightingAnime.StopLighting();
        yield break;
    }

    public void CreateParticle()
    {
        GameObject obj;
        particleTouchCount++;
        particleTouchCount %= particleMaxTouchCount;

        if(particleTouchCount == 0)
        {
            obj = PoolingManager.Instance.GetPool(particlePoolingName);
            if (obj == null)
                _ = Instantiate(partyParticle);
        }
        AudioManager.Instance.PlaySound(AudioManager.AudioKinds.PARTYBTN, Vector3.zero);
    }
}
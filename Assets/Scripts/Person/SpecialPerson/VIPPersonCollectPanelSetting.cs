﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VIPPersonCollectPanelSetting : MonoBehaviour
{
    [Header("사람 얼굴 이미지")]
    public Image faceImg;
    [Header("이름 빠빠빠빰")]
    public Text personName;
    [Header("팝업 매니져")]
    public PopupManager PM;

    [Header("열리는 시간")]
    public float animeTime;
    [Header("컨트롤러")]
    public VIPPersonFirstCollect controller;


    private Vector3 popup01, popup10, popup11, popup00;

    private RectTransform rect;
    private WaitForSeconds wait;

    private void Awake()
    {
        popup01 = new Vector3(0.025f, 1);
        popup10 = new Vector3(1, 0.025f);
        popup11 = new Vector3(1, 1);
        rect = GetComponent<RectTransform>();
        wait = new WaitForSeconds(animeTime);
    }

    private void Start()
    {

    }

    public void InfoSetting(VIPPersonElementInfo info)
    {
        faceImg.sprite = info.FaceSprite;
        OpenCollectPanel();
    }

    public void OpenCollectPanel()
    {
        StartCoroutine(_OpenCollectPanel());
    }

    public void CloseCollectPanel()
    {
        StartCoroutine(_CloseCollectPanel());
    }

    private IEnumerator _OpenCollectPanel()
    {
        rect.DOScale(popup01, animeTime).SetEase(Ease.OutExpo);
        yield return wait;
        rect.DOScale(Vector3.one, animeTime).SetEase(Ease.OutExpo);
        yield break;
    }

    private IEnumerator _CloseCollectPanel()
    {
        rect.DOScale(popup10, animeTime).SetEase(Ease.OutExpo);
        yield return wait;
        rect.DOScale(Vector3.zero, animeTime).SetEase(Ease.OutExpo);
        yield return wait;
        controller.StartCollectPanel();
        yield break;
    }
}

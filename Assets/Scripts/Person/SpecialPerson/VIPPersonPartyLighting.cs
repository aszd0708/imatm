﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VIPPersonPartyLighting : MonoBehaviour
{
    [Header("라이팅 이미지")]
    public Image[] lightImg;

    [Header("색과 밝기 컬러 변수들")]
    private Color red, green, blue;
    private Color[] colors = new Color[3];

    [Header("밝기 변수 : 1이 최대")]
    public float bright;

    [Header("조명 시간")]
    public float changeTime;

    private WaitForSeconds wait;
    private Coroutine lightingCor;

    private void Awake()
    {
        red = new Color(1, 0, 0, bright);
        green = new Color(0, 1, 0, bright);
        blue = new Color(0, 0, 1, bright);
        colors[0] = red; colors[1] = green; colors[2] = blue;
        wait = new WaitForSeconds(changeTime);
    }

    public void Lighting()
    {
        int a = Random.Range(0, 2);
        //Debug.Log("시작하는 모션 : " + a);
        a = 1;
        if (a == 0)
            LeftRightLighting();
        else
            ZigZagLighting();
    }

    private void LeftRightLighting()
    {
        lightingCor = StartCoroutine(_LeftRightLighting());
    }


    private IEnumerator _LeftRightLighting()
    {
        AllColorStting();

        List<Image> leftImg = new List<Image>();
        List<Image> rightImg = new List<Image>();
        int temp = 0;
        for(int i = 0; i < lightImg.Length; i++, temp++)
        {
            if (temp > 7)
                leftImg.Add(lightImg[i]);
            else if (temp < 7)
                rightImg.Add(lightImg[i]);
            else continue;

            if (temp == 13)
                temp = 0;
        }

        while(true)
        {
            for(int i = 0; i < leftImg.Count; i++)
            {
                leftImg[i].enabled = true;
                leftImg[i].color = SingleColorSetting(leftImg[i].color);
            }
            yield return wait; 
            for (int i = 0; i < leftImg.Count; i++)
                leftImg[i].enabled = false;

            for (int i = 0; i < rightImg.Count; i++)
            {
                rightImg[i].enabled = true;
                rightImg[i].color = SingleColorSetting(rightImg[i].color);
            }
            yield return wait; 
            for (int i = 0; i < rightImg.Count; i++)
                rightImg[i].enabled = false;
        }
    }

    private void ZigZagLighting()
    {
        lightingCor = StartCoroutine(_ZigZagLighting());
    }
    private IEnumerator _ZigZagLighting()
    {
        AllColorStting();
        List<Image> zig = new List<Image>();
        List<Image> zag = new List<Image>();
        for (int i = 0; i < lightImg.Length; i++)
        {
            int temp = i % 2;

            if (temp == 0)
                zig.Add(lightImg[i]);
            else
                zag.Add(lightImg[i]);
        }

        while(true)
        {
            for(int i = 0; i < zig.Count; i++)
            {
                zig[i].enabled = true;
                zig[i].color = SingleColorSetting(zig[i].color);
            }
            yield return wait;
            for (int i = 0; i < zig.Count; i++)
                zig[i].enabled = false;
            for(int i = 0; i < zag.Count; i++)
            {
                zag[i].enabled = true;
                zag[i].color = SingleColorSetting(zag[i].color);
            }
            yield return wait;
            for (int i = 0; i < zag.Count; i++)
                zag[i].enabled = false;
        }
    }

    private void AllColorStting()
    {
        for (int i = 0, temp = 0; i < lightImg.Length; i++,temp++)
        {
            temp %= colors.Length;
            lightImg[i].color = colors[temp];
            lightImg[i].enabled = false;
        }
    }

    private Color SingleColorSetting(Color color)
    {
        //if (color == red)
        //    return green;
        //else if (color == green)
        //    return blue;
        //else if (color == blue)
        //    return red;
        //else
        //    return red;

        return new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f), bright);
    }

    public void StopLighting()
    {
        StopCoroutine(lightingCor);
        for (int i = 0, temp = 0; i < lightImg.Length; i++, temp++)
            lightImg[i].enabled = false;
    }
}

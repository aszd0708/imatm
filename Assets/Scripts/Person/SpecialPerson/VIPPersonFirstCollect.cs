﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VIPPersonFirstCollect : MonoBehaviour
{
    [Header("수집 판넬")]
    public VIPPersonCollectPanelSetting collectPanel;
    [Header("게임 오버 매니져")]
    public StartOverManager GM;

    [Header("요소 클래스 리스트")]
    private Queue<VIPPersonElementInfo> personInformation = new Queue<VIPPersonElementInfo>();
    public Queue<VIPPersonElementInfo> PersonInformation 
    {
        get
        {
            return personInformation;
        }

        set
        {
            personInformation = value;
        }
    }

    private int pointer = 0;
    public int Pointer { get => pointer; set => pointer = value; }

    public void StartCollectPanel()
    {
        //Debug.Log("실행");
        if (Pointer == PersonInformation.Count)
        {
            GM.VipCollectGameOver();
            PersonInformation.Clear();
            Pointer = 0;
            return;
        }
        collectPanel.InfoSetting(PersonInformation.Dequeue());
    }
}

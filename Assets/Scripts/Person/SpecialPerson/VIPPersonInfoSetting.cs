﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

interface IVIPPerson
{
    void CreateSetting(Sprite faceSprite, int _score);
}

[System.Serializable]
public class VIPPersonElementInfo
{
    private Sprite faceSprite;
    private int score;

    public Sprite FaceSprite { get => faceSprite; set => faceSprite = value; }
    public int Score { get => score; set => score = value; }
}

public class VIPPersonInfoSetting : MonoBehaviour, IPoolingObject, IVIPPerson
{
    [Header("얼굴 스프라이트 랜더러")]
    public SpriteRenderer faceRenderer;

    private VIPPersonController controller;

    private string poolingName;

    public string PoolingName { get => poolingName; set => poolingName = value; }

    [Header("각 요소 인포메이션 클래스")]
    private VIPPersonElementInfo info = new VIPPersonElementInfo();
    public VIPPersonElementInfo Info { get => info; set => info = value; }

    public string poolingObjName;

    private void Awake()
    {
        controller = FindObjectOfType<VIPPersonController>();
    }

    public void CreateSetting(Sprite faceSprite, int _score)
    {
        faceRenderer.sprite = faceSprite;
        Info.FaceSprite = faceSprite;
        Info.Score = _score;

    }

    private void Start()
    {
        PoolingName = "VIP";
    }

    private void OnDisable()
    {
        Info = new VIPPersonElementInfo();
    }

    public void SendPersonInfo()
    {
        controller.BonusStart(Info.Score);
        controller.ElementInfo.Add(info);
    }
}

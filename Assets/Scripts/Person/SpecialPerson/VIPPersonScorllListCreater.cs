﻿using Mosframe;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VIPPersonScorllListCreater : MonoBehaviour
{
    [Header("만들 UI 리스트")]
    public GameObject infoList;
    [Header("인포들")]
    public VIPPersonInfoLoader info;
    [Header("스크롤 리스트")]
    public RectTransform scrollUI;
    [Header("일단 기본으로 쓸 이미지")]
    public Sprite nullSprite;
    [Header("VIP 이미지들")]
    public Sprite[] personSprites;
    [Header("데이터")]
    public UserDataSaveLoad userData;
    [Header("만든 리스트들")]
    public List<GameObject> list = new List<GameObject>();
    [Header("위치 조정")]
    public VerticalLayoutGroup layout;

    private void Awake()
    {
        CreateList();
        SetDefault(userData.UserData.SelectFirstVipPerson);
    }

    private void CreateList()
    {
        for (int i = 0; i < info.PeopleInfo.Count; i++)
        {
            GameObject infoUI = Instantiate(infoList);
            infoUI.GetComponent<IVipScrollList>().InfoSetting(i, nullSprite, personSprites[i], info.PeopleInfo[i].OpenScore, this);
            infoUI.transform.SetParent(scrollUI);
            RectTransform rect = infoUI.GetComponent<RectTransform>();
            rect.sizeDelta = new Vector2(864, rect.sizeDelta.y);
            rect.anchoredPosition = new Vector2(0, -rect.sizeDelta.y * i);
            rect.GetComponent<IVipScrollList>().IsCollect(false);
            rect.GetComponent<IVipScrollList>().FirstCollcet(false);
            rect.GetComponent<Button>().enabled = false;
            list.Add(infoUI);
            StartCoroutine(SetSize(rect));
        }

        for(int i = 0; i < userData.UserData.VipPersonCollect; i++)
        {
            list[i].GetComponent<IVipScrollList>().IsCollect(true);
            list[i].GetComponent<Button>().enabled = true;
        }
    }

    public void OpenInfo()
    {
        for (int i = 0; i < list.Count; i++)
        {
            list[i].GetComponent<IVipScrollList>().IsCollect(false);
            list[i].GetComponent<Button>().enabled = false;
        }

        for (int i = 0; i < userData.UserData.VipPersonCollect; i++)
        {
            list[i].GetComponent<IVipScrollList>().IsCollect(true);
            list[i].GetComponent<Button>().enabled = true;
        }
    }

    public void SetDefault(int selectIndex)
    {
        for (int i = 0; i < list.Count; i++)
        {
            VIPPersonListUISetting listInfo = list[i].GetComponent<VIPPersonListUISetting>();
            // 선택 안한것들
            //list[i].GetComponent<Image>().color = Color.black;
            listInfo.borderImg.enabled = false;
            listInfo.checkImg.enabled = false;
        }
        if (selectIndex != -1)
        {
            // 선택한것
            //list[selectIndex].GetComponent<Image>().color = Color.white;
            VIPPersonListUISetting listInfo = list[selectIndex].GetComponent<VIPPersonListUISetting>();
            listInfo.borderImg.enabled = true;
            listInfo.checkImg.enabled = true;
        }

        userData.UserData.SelectFirstVipPerson = selectIndex;
        userData.SaveData();
    }

    public void ClickBtn(int selectIndex)
    {
        if(userData.UserData.SelectFirstVipPerson == selectIndex)
            selectIndex = -1;

        for (int i = 0; i < list.Count; i++)
        {
            VIPPersonListUISetting listInfo = list[i].GetComponent<VIPPersonListUISetting>();
            // 선택 안한것들
            //list[i].GetComponent<Image>().color = Color.black;
            listInfo.borderImg.enabled = false;
            listInfo.checkImg.enabled = false;
        }
        if(selectIndex != -1)
        {
            // 선택한것
            //list[selectIndex].GetComponent<Image>().color = Color.white;
            VIPPersonListUISetting listInfo = list[selectIndex].GetComponent<VIPPersonListUISetting>();
            listInfo.borderImg.enabled = true;
            listInfo.checkImg.enabled = true;
        }

        userData.UserData.SelectFirstVipPerson = selectIndex;
        userData.SaveData();
    }

    private IEnumerator SetSize(RectTransform rect)
    {
        yield return new WaitForSeconds(0.3f);
        rect.localScale = Vector2.one;
        yield break;
    }

    public void OpenVIP(int index)
    {
        list[index].GetComponent<IVipScrollList>().IsCollect(true);
        list[index].GetComponent<Button>().enabled = true;
    }
}

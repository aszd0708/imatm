﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VIPPersonListUISetting : MonoBehaviour, IVipScrollList
{
    [Header("VIP이미지")]
    public Image vipImg;

    [Header("얼굴")]
    public Image faceImg;

    [Header("스코어?")]
    public Text openScoreText;

    [Header("인덱스")]
    public int index;

    [Header("테두리")]
    public Image borderImg;
    [Header("체크 마크")]
    public Image checkImg;
    [Header("뉴 마크")]
    public GameObject newMark;

    [Header("물음표 텍스트")]
    public Text isCollectText;

    private Button btn;

    private VIPPersonScorllListCreater vipListSetting;

    public VIPPersonScorllListCreater VipListSetting { get => vipListSetting; set => vipListSetting = value; }

    private void Awake()
    {
        btn = GetComponent<Button>();
    }

    public void InfoSetting(int _index, Sprite vipSprite, Sprite faceSprite, string openScore, VIPPersonScorllListCreater _VipListSetting)
    {
        index = _index;
        vipImg.sprite = vipSprite;
        faceImg.sprite = faceSprite;
        openScoreText.text = openScore;
        VipListSetting = _VipListSetting;
    }

    public void BtnClick()
    {
        AudioManager.Instance.PlaySound(AudioManager.AudioKinds.VIPSELECT, Vector3.zero);
        VipListSetting.ClickBtn(index);
        FirstCollcet(false);
    }

    public void IsCollect(bool isCollect)
    {
        if (isCollect)
        {
            faceImg.color = Color.white;
            isCollectText.enabled = false;
        }

        else
        {
            faceImg.color = Color.black;
            isCollectText.enabled = true;
        }
    }

    public void FirstCollcet(bool isFirstCollect)
    {
        if (isFirstCollect)
        {
            //newMark.SetActive(true);
            btn.enabled = true;
        }
        else newMark.SetActive(false);
    }
}
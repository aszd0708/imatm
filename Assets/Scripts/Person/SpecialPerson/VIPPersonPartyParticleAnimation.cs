﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VIPPersonPartyParticleAnimation : MonoBehaviour, IPoolingObject
{
    [Header("위치 최대 최소")]
    public float maxX;
    public float minX, dropY;

    [Header("날아가는 힘 최대 최소")]
    public float maxPow;
    public float minPow;

    [Header("날아가는 시간")]
    public float maxDropTime;
    public float minDropTime;

    [Header("만들어지는 위치")]
    public Vector3 createPos;

    private string pooingName;
    public string PoolingName { get => pooingName; set => pooingName = value; }
    private SpriteRenderer sprite;

    private void Awake()
    {
        sprite = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        PoolingName = "ParticleObj";
    }

    private void OnEnable()
    {
        transform.position = createPos;
        MotionStart();
    }

    private void MotionStart()
    {
        float destX = Random.Range(minX, maxX);
        float dropTime = Random.Range(minDropTime, maxDropTime);

        sprite.flipX = SetRandomBool();
        sprite.flipY = SetRandomBool();
        transform.rotation = Quaternion.Euler(0, 0, Random.Range(0, 360));
        transform.DOJump(new Vector3(destX, dropY), Random.Range(minPow, maxPow), 1, dropTime);
        Invoke("Pool", dropTime);
    }

    private void Pool()
    {
        PoolingManager.Instance.SetPool(gameObject, PoolingName);
    }

    private bool SetRandomBool()
    {
        bool randomBool = false;
        int randomInt = Random.Range(0, 2);
        switch (randomInt)
        {
            case 0: randomBool = false; break;
            case 1: randomBool = true; break;
        }
        return randomBool;
    }
}

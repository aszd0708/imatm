﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * 2020-06-02
 * 이상한 사람
 * 진짜 이상하다
 * 만들기 싫을 정도로 이상하다
 * 하....
 * 피버를 열어주는 사람?? 이라고 보면 편함
 */

public class VIPPersonController : MonoBehaviour
{
    [Header("Vip 프리팹")]
    public GameObject vipPerson;

    [Header("다른 시간 멈추게 하는 매니저")]
    public TimeManager timeManager;

    [Header("터치 할 버튼들")]
    public GameObject touchBtn;

    [Header("돈 버튼")]
    public GameObject moneyBtn;

    [Header("줄 컨트롤러")]
    public PeopleLineController lineController;

    [Header("점수 컨트롤러")]
    public ScoreManager scoreManager;

    [Header("어떤 VIP님이 오실지 데이터")]
    public UserDataSaveLoad userData;

    [Header("Json Loader")]
    public VIPPersonInfoLoader info;

    [Header("판넬 콜렉트")]
    public VIPPersonFirstCollect firstCol;

    [Header("VIP 얼굴들")]
    public Sprite[] vipFaces;

    public StartOverManager GM;

    [Header("타이머 전환시 시간")]
    public int timerChangeTime;

    [Header("타이머 전환 텍스트")]
    public Text timerChangeTimeText;

    [Header("파티 예아")]
    public VIPPersonEventParty partyController;

    private WaitForSeconds timerChangeTimeWait;
    private WaitForSeconds oneTimeWait;

    private VIPPersonTimer timer;

    private int score;
    public int Score { get => score; set => score = value; }

    private bool firstBonus;
    public bool FirstBonus { get => firstBonus; set => firstBonus = value; }

    private List<VIPPersonElementInfo> elementInfo = new List<VIPPersonElementInfo>();
    public List<VIPPersonElementInfo> ElementInfo { get => elementInfo; set => elementInfo = value; }

    public PeopleCreater peopleCreater;

    private void Awake()
    {
        timer = GetComponent<VIPPersonTimer>();
        timerChangeTimeWait = new WaitForSeconds(timerChangeTime);
        oneTimeWait = new WaitForSeconds(1.0f);
        timerChangeTimeText.enabled = false;
    }

    public void BonusSetting()
    {
        if (userData.UserData.SelectFirstVipPerson == -1) FirstBonus = false;
        else FirstBonus = true;
    }

    public void BonusStart(int value)
    {
        timeManager.Pause(true);
        touchBtn.SetActive(true);
        moneyBtn.SetActive(false);
        partyController.FeverStart();
        Score = value;
        Invoke("TimerStart", 0.1f);
        if (!FirstBonus)
            userData.UserData.VipPersonCollect++;
    }

    private void TimerStart()
    {
        timer.TimerStart();
    }

    public void BonusStop()
    {
        StartCoroutine(_BonusStop());
    }

    private IEnumerator _BonusStop()
    {
        touchBtn.SetActive(false);
        lineController.SendMessage("Answer", true, SendMessageOptions.DontRequireReceiver);
        timeManager.SendMessage("TimerStop", SendMessageOptions.DontRequireReceiver);
        // 대충 4,3,2,1 타이머 나온다는 함수
        timerChangeTimeText.enabled = true;
        for (int i = 0; i < timerChangeTime; i++)
        {
            timerChangeTimeText.text = string.Format("{0}", timerChangeTime - i);
            yield return oneTimeWait;
        }

        timerChangeTimeText.enabled = false;
        NormalTimerStart();
        peopleCreater.CanICreateVIP = true;
        yield break;
    }

    private void NormalTimerStart()
    {
        moneyBtn.SetActive(true);

        timeManager.SendMessage("ResetTimer", SendMessageOptions.DontRequireReceiver);
        timeManager.Pause(false);

        if (!FirstBonus)
        {
            firstCol.PersonInformation.Enqueue(ElementInfo[0]);
            GM.CollectVIP = true;
            ElementInfo.RemoveAt(0);
        }
        else
        {
            FirstBonus = false;
            ElementInfo.RemoveAt(0);
        }
    }

    public void ScorePlus()
    {
        scoreManager.PlusScore(Score);
    }

    public GameObject GetVipPerson()
    {
        GameObject person = Instantiate(vipPerson);
        int index = userData.UserData.VipPersonCollect;
        //Debug.Log("VIP 인덱스 : " + userData.UserData.VipPersonCollect);
        person.GetComponent<IVIPPerson>().CreateSetting(vipFaces[index], info.PeopleInfo[index].ScoreInt);
        //Debug.Log("VIP 얼굴 이름 : " + info.PeopleInfo[index].FaceName);
        info.PeopleInfo[index].PrintInfo();
        return person;
    }

    public void SetVipPerson(GameObject person)
    {
        int index = userData.UserData.VipPersonCollect;
        person.GetComponent<IVIPPerson>().CreateSetting(vipFaces[index], info.PeopleInfo[index].ScoreInt);
        info.PeopleInfo[index].PrintInfo();
    }

    public GameObject GetFirstVIPPerson()
    {
        GameObject person;
        person = PoolingManager.Instance.GetPool("VIP");
        if (person == null)
            person = Instantiate(vipPerson);
        int index = userData.UserData.SelectFirstVipPerson;
        //Debug.Log("인덱스 : " + userData.UserData.SelectFirstVipPerson);
        person.GetComponent<IVIPPerson>().CreateSetting(vipFaces[index], info.PeopleInfo[index].ScoreInt);
        return person;
    }

    public bool CanGetVipPerson(int index)
    {
        bool canGet;

        //Debug.Log("Index : " + index);
        //Debug.Log("PersonScore : " + info.PeopleInfo[index].OpenScoreInt);
        if (index >= info.PeopleInfo.Count)
        {
            canGet = false;
        }
        else
        {
            if (scoreManager.Score >= info.PeopleInfo[index].OpenScoreInt) canGet = true;
            else canGet = false;
        }
       // Debug.Log("Can I Get VIP?? : " + canGet);
        return canGet;
    }
}
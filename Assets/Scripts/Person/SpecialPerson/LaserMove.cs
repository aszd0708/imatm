﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserMove : MonoBehaviour
{
    private int x = 0;
    private int y = 1;
    [SerializeField]
    public float[] angle = new float[2];
    public float[] center = new float[2];
    public float[] speed = new float[2];
    public float range;
    private float xPos;
    private float yPos;
    private float[] originPos = new float[2];

    void Start()
    {
        originPos[x] = transform.position.x;
        originPos[y] = transform.position.y;
    }

    void Update()
    {
        //  * range
        xPos = center[x] + Mathf.Sin(angle[x]);
        yPos = center[y] + Mathf.Sin(angle[y]);

        angle[x] += speed[x];
        angle[y] += speed[y];

        transform.position = new Vector3(originPos[x] + xPos, originPos[y] + yPos, transform.position.z);
        transform.localScale = new Vector3(Mathf.Sin(angle[y]) * 2f, Mathf.Sin(angle[y]) * 2f);
    }
}

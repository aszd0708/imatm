﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class VIPPersonInfo
{
    public string VIP_Rating;
    public string Score;
    public string FaceName;
    public string OpenScore;

    private int scoreInt;
    private int openScoreInt;

    public int OpenScoreInt
    {
        get
        {
            openScoreInt = System.Convert.ToInt32(OpenScore);
            return openScoreInt;
        }

        set => OpenScoreInt = value;
    }

    public int ScoreInt
    {
        get
        {
            scoreInt = System.Convert.ToInt32(Score);
            return scoreInt;
        }

        set => ScoreInt = value;
    }

    public void PrintInfo()
    {
        //Debug.Log("등급 : " + VIP_Rating + " 점수 : " + Score + " 파일명 : " + FaceName + " 오픈 스코어 : " + OpenScore);
    }
}

public class VIPPersonInfoLoader : JsonDataLoadManager<VIPPersonInfo>
{
    private List<VIPPersonInfo> peopleInfo = new List<VIPPersonInfo>();
    public List<VIPPersonInfo> PeopleInfo { get => peopleInfo; set => peopleInfo = value; }

    [Header("제이슨 파일")]
    public TextAsset json;

    private void Awake()
    {
        PeopleInfo = LoadJson(json);

        for (int i = 0; i < PeopleInfo.Count; i++)
            PeopleInfo[i].PrintInfo();
    }
}
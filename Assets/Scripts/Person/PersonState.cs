﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PersonState : MonoBehaviour, IPerson
{
    [Header("원하는 돈")]
    public int money;
    [Header("가격 텍스트")]
    public TextMeshPro textMesh;

    [Header("사람 얼굴들")]
    public Sprite[] faces;

    [Header("지금 웨이브 레벨")]
    private int nowWaveLevel;
    public int NowWaveLevel { get => nowWaveLevel; set => nowWaveLevel = value; }

    [Header("돈 가격들")]
    private int[] moneies = new int[4];
    public int[] Moneies { get => moneies; set => moneies = value; }


    public SpriteRenderer sprite;

    protected PeopleLineController peopleController;
    protected PlayerController playerController;
    protected PlayerLifeController playerLifeController;

    private WaveLevelController waveController;

    private void Awake()
    {
        peopleController = FindObjectOfType<PeopleLineController>();
        playerController = FindObjectOfType<PlayerController>();
        playerLifeController = FindObjectOfType<PlayerLifeController>();
        waveController = FindObjectOfType<WaveLevelController>();
        Moneies = playerController.moneies;
    }

    private void OnEnable()
    {
        RandomVisual();
        RandomMoney();
    }

    private void SetLevel(int level)
    {
        NowWaveLevel = level;
    }

    public void RandomVisual()
    {
        //Color randomColor = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
        //sprite.color = randomColor;
        sprite.sprite = faces[Random.Range(0, faces.Length)];
    }

    public void RandomMoney()
    {
        List<int> touchCount = waveController.GetTouchCount();
        List<int> percent = waveController.GetPercent();

        //Debug.Log(gameObject.GetInstanceID());

        int random = RandomValue.GetRandomValue(touchCount, percent);

        int index = 0;
        for(int i = 0; i < random; i++)
        {
            index += Moneies[Random.Range(0, Moneies.Length)];
        }
        money = index;
        SetPriceText();
    }

    public void SetPriceText()
    {
        textMesh.text = string.Format("{0:#,###}", money);
    }

    public void SendPersonInfo()
    {
        // 가장 앞에 있는 사람의 정보를 넘겨줘 플레이어가 돈을 낼 수 있게 해주는 함수
        playerController.PersonState = this; 
    }
}

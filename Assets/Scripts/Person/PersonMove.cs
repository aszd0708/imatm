﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PersonMove : MonoBehaviour
{
    [Header("움직일 오브젝트")]
    public Transform moveObj;

    public float[] randomTimeRange = new float[2];

    private bool nowFirst;
    private bool isMove;

    public Vector3[] rotationRange = new Vector3[2];

    private void OnEnable()
    {
        StartCoroutine(_IdleMotion());
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    public bool NowFirst { get => nowFirst; set => nowFirst = value; }

    public void MoveToPose(Vector3 movePose, float moveTime)
    {
        StartCoroutine(_MoveToPose(movePose, moveTime));
    }

    private IEnumerator _MoveToPose(Vector3 movePose, float moveTime)
    {
        MoveMotion(moveTime);
        yield return new WaitForSeconds(moveTime / 2);
        transform.DOMove(movePose, moveTime/2);
        yield break;
    }

    private void MoveMotion(float moveTime)
    {
        StartCoroutine(_MoveMotion(moveTime));
    }
    private IEnumerator _MoveMotion(float moveTime)
    {
        moveObj.DOScale(new Vector3(1, 0.8f, 1), moveTime / 2);
        yield return new WaitForSeconds(moveTime / 2);
        moveObj.DOScale(new Vector3(1, 1, 1), moveTime / 2).SetEase(Ease.OutBounce);
        yield break;
    }

    private IEnumerator _IdleMotion()
    {
        float randomIdleTime = Random.Range(randomTimeRange[0], randomTimeRange[1]);
        int index = 0;
        Ease ease = (Ease)Random.Range(0, 35);
        while (true)
        {
            moveObj.DOLocalRotate(rotationRange[index++], randomIdleTime).SetEase(ease);
            index %= rotationRange.Length;
            yield return new WaitForSeconds(randomIdleTime);
        }    
    }
}

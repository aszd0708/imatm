﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonBasicScript : PersonState, IPoolingObject
{
    private string poolingName;

    public string PoolingName { get => poolingName; set => poolingName = value; }

    private void Start()
    {
        PoolingName = "BasicPerson";
    }

    public void Answer(PersonStateEnum state)
    {
        switch (state)
        {
            case PersonStateEnum.ANGER: 
                peopleController.SendMessage("Answer", false, SendMessageOptions.DontRequireReceiver);
                break;
            case PersonStateEnum.HAPPY:
                peopleController.SendMessage("Answer", false, SendMessageOptions.DontRequireReceiver);
                break;
            case PersonStateEnum.NORMAL:
                peopleController.SendMessage("Answer", true, SendMessageOptions.DontRequireReceiver);
                break;
            default:
                break;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonHaveLifeScript : PersonState, IPoolingObject
{
    private string poolingName;

    public string PoolingName { get => poolingName; set => poolingName = value; }

    private void Start()
    {
        PoolingName = "HaveLifePerson";
    }

    public void Answer(PersonStateEnum state)
    {
        switch (state)
        {
            case PersonStateEnum.ANGER:
                print("개빡침");
                peopleController.SendMessage("Answer", false, SendMessageOptions.DontRequireReceiver);
                break;
            case PersonStateEnum.HAPPY:
                print("개행복함");
                peopleController.SendMessage("Answer", false, SendMessageOptions.DontRequireReceiver);
                break;
            case PersonStateEnum.NORMAL:
                print("그냥 그저 그럼");
                peopleController.SendMessage("Answer", true, SendMessageOptions.DontRequireReceiver);
                playerLifeController.SendMessage("PlusLife", SendMessageOptions.DontRequireReceiver);
                break;
            default:
                break;
        }
    }
}

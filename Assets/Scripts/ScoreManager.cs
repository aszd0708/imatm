﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    [Header("점수 Text Object")]
    public Text scoreText;
    [Header("최대 점수 MaximunScore Text")]
    public Text maximumText;

    [Header("스코어")]
    private int score;

    [Header("터치당 몇점 줄지 결정")]
    public int giveScore;

    [Header("웨이브 레벨 껀트롤러")]
    public WaveLevelController waveLevelController;
    [Header("제이슨 데이타")]
    public WaveLevelSettingManager jsonData;

    [Header("최대 스코어 파일")]
    public UserDataSaveLoad userData;
    public int Score { get => score; set => score = value; }

    private void Start()
    {
        StartScore();
    }

    private void CalScore(int touchCount)
    {
        //int value = touchCount * giveScore;
        int value = jsonData.WaveLevelInfos[waveLevelController.NowLevel - 1].ScoreInt;
        Score += value;
        SetText(scoreText, Score);

        //Debug.Log("점수 : " + Score + " 최대점수 : " + userData.UserData.MaximumScore);
        if (userData.UserData.MaximumScore <= Score)
            SetText(maximumText, Score);
    }

    private void StartScore()
    {
        //Debug.Log(" 최대점수 : " + userData.UserData.MaximumScore);
        SetText(maximumText, userData.UserData.MaximumScore);
        SetText(scoreText, Score);
    }

    public void StartScore(int inScore)
    {
        Score = inScore;
        SetText(maximumText, userData.UserData.MaximumScore);
        SetText(scoreText, Score);
    }

    private void ResetScore()
    {
        if (userData.UserData.MaximumScore <= Score)
        {
            userData.UserData.MaximumScore = Score;
            Social.ReportScore(Score, GPGSIds.leaderboard, success => { /*Debug.Log("리더보드 연결");*/ });
        }
        Score = 0;
        //Debug.Log(" 최대점수 : " + userData.UserData.MaximumScore);
        SetText(maximumText, userData.UserData.MaximumScore);
        SetText(scoreText, Score);
    }

    public void PlusScore(int value)
    {
        Score += value;
        SetText(scoreText, Score);

        if (userData.UserData.MaximumScore <= Score)
            SetText(maximumText, Score);
    }

    private void SetText(Text text, int value)
    {
        //text.text = string.Format("{0}", value);
        StartCoroutine(_Count(value, System.Convert.ToInt32(text.text), text));
    }

    IEnumerator _Count(float target, float current, Text text)
    {
        float duration = 0.3f; // 카운팅에 걸리는 시간 설정. 
        float offset = (target - current) / duration;
        while (current < target)
        {
            current += (offset * Time.deltaTime);
            text.text = ((int)current).ToString();
            yield return null;
        }
        current = target;
        text.text = ((int)current).ToString();
        yield break;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeManager : MonoBehaviour
{
    [Header("타이머UI")]
    public Image timer;

    [Header("타이머 Text")]
    public Text timerText;

    [Header("최대 시간")]
    public float maxTime;

    [Header("줄이 앞당겨지는 시간 체크하기 위해 넣어둠")]
    public PeopleMoveController move;

    [Header("사람마다 주는 값")]
    public float lessTime;

    [Header("주는 사람 단위")]
    public int lessPersonCount;

    [Header("지금까지 해결한 사람들의 숫자")]
    private int peopleCount;

    public int PeopleCount { get => peopleCount; set => peopleCount = value; }

    private float nowTime;
    public float NowTime { get => nowTime; set => nowTime = value; }

    private float plusTime;

    private GameManager GM;

    private Coroutine timerCor;
    private WaitForSeconds wait;
    private StartOverManager startOverManager;
    private PlayerLifeController playerLifeController;
    private PlayerController playerController;
    private WaveLevelController waveController;

    public float timeSoundStartTime = 4.3f;

    private bool isPause = false;
    public bool IsPause { get => isPause; set => isPause = value; }

    // 타이머 timeSoundStartTime초정도 남았을때 나오는 오디오
    // 알아서 조정하게 만들거임
    private AudioSource timerLessAudio;

    // 시험용 나중에 지울것
    public Text timeText;

    public Button upButton, downButton;

    private void Awake()
    {
        startOverManager = GetComponent<StartOverManager>();
        waveController = GetComponent<WaveLevelController>();
        GM = GetComponent<GameManager>();

        playerLifeController = FindObjectOfType<PlayerLifeController>();
        playerController = FindObjectOfType<PlayerController>();
    }

    private void Start()
    {
        NowTime = maxTime;
        wait = new WaitForSeconds(move.moveTime);
    }

    private void TimerStart()
    {
        SetTime();
        IsPause = false;
        NowTime = plusTime;
        timerCor = StartCoroutine(_Timer());
    }

    public void ResetTimer()
    {
        StartCoroutine(_ResetTimer());
    }

    private void TimerStop()
    {
        if (timerCor != null)
            StopCoroutine(timerCor);
        NowTime = maxTime;
        timerText.text = string.Format("{0:f2}", 0);
        timer.fillAmount = NowTime / maxTime;
        timer.color = new Color(1, timer.fillAmount, timer.fillAmount);
        Pause(true);
    }

    public void Pause(bool pause)
    {
        IsPause = pause;
        if (pause)
        {
            if (timerCor != null)
            {
                StopCoroutine(timerCor);
                timerCor = null;
            }
        }
        else
        {
            if (timerCor == null)
                timerCor = StartCoroutine(_Timer());
        }
    }

    private IEnumerator _Timer()
    {
        while (NowTime >= 0)
        {
            if (GM.IsGameOver)
                yield break;
            NowTime -= Time.deltaTime;
            timer.fillAmount = NowTime / maxTime;
            timer.color = new Color(1, timer.fillAmount, timer.fillAmount);
            timerText.text = string.Format("{0:f2}", NowTime);
            if (NowTime <= timeSoundStartTime)
            {
                if (timerLessAudio == null)
                    timerLessAudio = AudioManager.Instance.GetPlaySound(AudioManager.AudioKinds.LESSTIME, Vector3.zero);
            }

            if (IsPause)
            {
                if (timerLessAudio == null)
                    timerLessAudio = AudioManager.Instance.GetPlaySound(AudioManager.AudioKinds.LESSTIME, Vector3.zero);
                timerLessAudio.Stop();
                yield break;
            }

            yield return null;
        }
        // 대충 실패했다는 함수 실행
        playerLifeController.SendMessage("TimeOut", SendMessageOptions.DontRequireReceiver);
        yield break;
    }

    private IEnumerator _ResetTimer()
    {
        if (timerCor != null)
            StopCoroutine(timerCor);
        if (timerLessAudio != null)
        {
            GameObject timerAudio = timerLessAudio.gameObject;
            timerLessAudio = null;
            timerAudio.SetActive(false);
        }
        SetTime();
        float timeResetSpeed = (plusTime - NowTime) / move.moveTime;
        while (plusTime > NowTime)
        {
            NowTime += Time.deltaTime * timeResetSpeed;
            timer.fillAmount = NowTime / maxTime;
            timer.color = new Color(1, timer.fillAmount, timer.fillAmount);
            timerText.text = string.Format("{0:f2}", NowTime);
            yield return null;
        }
        TimerStart();
        IsPause = false;
        yield break;
    }

    private void SetTime()
    {
        float lessValue = maxTime - ((PeopleCount / lessPersonCount) * lessTime);
        if (lessValue <= 2f)
            lessValue = 2f;
        plusTime = NowTime + lessValue;
        if (plusTime >= maxTime)
            plusTime = maxTime;
    }

    public void TimeBtnSetting(bool up)
    {
        if (up)
            lessPersonCount += 1;
        else
            lessPersonCount -= 1;

        if (lessPersonCount <= 1)
            lessPersonCount = 1;

        timeText.text = string.Format("{0}", lessPersonCount);
    }
}
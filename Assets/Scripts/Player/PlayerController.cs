﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    [Header("지금가지 낸 가격 보여주는 TextMesh")]
    public Text text;

    private PlayerGiveMoneyController giveMoney;
    private TimeManager timeManager;

    private PersonState personState;
    public PersonState PersonState { get => personState; set => personState = value; }

    [Header("지금까지 해결한 사람들의 수")]
    private int peopleCount;

    public int PeopleCount { get => peopleCount; set => peopleCount = value; }

    [Header("돈 가격들")]
    public int[] moneies;

    private ScoreManager scoreManager;
    private PlayerLifeController lifeController;
    private WaveLevelController waveController;

    private void Awake()
    {
        giveMoney = GetComponent<PlayerGiveMoneyController>();
        lifeController = GetComponent<PlayerLifeController>();

        timeManager = FindObjectOfType<TimeManager>();
        scoreManager = FindObjectOfType<ScoreManager>();
        waveController = FindObjectOfType<WaveLevelController>();
    }

    private void Start()
    {
        //timeManager.SendMessage("TimerStart", SendMessageOptions.DontRequireReceiver);
    }

    public void Give(int value)
    {
        if (giveMoney.GiveMoney(value))
        {
            text.text = string.Format("<color=#ffffff>\\ </color><color=#000000>{0}</color>", giveMoney.Money);
        }
        else return;
    }

    private void ShowMoney(int value)
    {
        text.text = string.Format("<color=#ffffff>\\ </color><color=#000000>{0}</color>", giveMoney.Money);
    }

    public void CheckingMoney()
    {
        if (PersonState.money == giveMoney.Money)
        {
            PersonState.SendMessage("Answer", PersonStateEnum.NORMAL, SendMessageOptions.DontRequireReceiver);
            scoreManager.SendMessage("CalScore", giveMoney.MoneyTouchCount, SendMessageOptions.DontRequireReceiver);
            timeManager.SendMessage("ResetTimer", SendMessageOptions.DontRequireReceiver);
            timeManager.PeopleCount++;
            AudioManager.Instance.PlaySound(AudioManager.AudioKinds.PAY, Vector3.zero);
        }
        else if (PersonState.money > giveMoney.Money)
        {
            PersonState.SendMessage("Answer", PersonStateEnum.ANGER, SendMessageOptions.DontRequireReceiver);
            lifeController.SendMessage("WrongAnswer", SendMessageOptions.DontRequireReceiver);
            VibratorManager.Instance.WrongMoney();
        }
        else if (PersonState.money < giveMoney.Money)
        {
            PersonState.SendMessage("Answer", PersonStateEnum.HAPPY, SendMessageOptions.DontRequireReceiver);
            lifeController.SendMessage("WrongAnswer", SendMessageOptions.DontRequireReceiver);
            VibratorManager.Instance.WrongMoney();
        }

        giveMoney.Money = 0;
        giveMoney.MoneyTouchCount = 0;

        text.text = string.Format("<color=#ffffff>\\ </color><color=#000000>{0}</color>", giveMoney.Money);
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * 2020-05-19
 * 플레이어의 라이프를 컨트롤 할 수 있게 해주는 스크립트
 * 최대 갯수는 5개고 처음에 주어지는 라이프 갯수는 3개
 */

public class PlayerLifeController : MonoBehaviour
{
    [Header("최대 라이프 갯수")]
    public int maxLife;

    [Header("처음에 주는 라이프 갯수")]
    public int startLife;

    [Header("라이프 이미지 부모")]
    public RectTransform lifeImagParent;

    [Header("지금 라이프 갯수")]
    private int nowLife;

    public PlayerGiveMoneyController moneyController;

    [Header("타이머 전환 텍스트")]
    public Text timerChangeTimeText;

    [Header("돈 패널")]
    public GameObject moneyPanel;

    public WaitForSeconds timerChangeTimeWait;
    public float timerChangeTime = 4;
    public int NowLife
    {
        get => nowLife; set
        {
            nowLife = value;
            if (nowLife == 0)
            {
                GameManager.Instance.IsGameOver = true;
                startOverManager.SendMessage("GameOver", SendMessageOptions.DontRequireReceiver);
            }
            else if (nowLife == 1)
            {
                // 대충 광고 보겠냐는 팝업
                PopupManager.Instance.OpenPopup(ref panel, true);
                AudioManager.Instance.PlaySound(AudioManager.AudioKinds.WRONG, Vector3.zero);
                timeManager.SendMessage("TimerStop", SendMessageOptions.DontRequireReceiver);
                moneyController.ResetValues();
            }
            else if (nowLife > maxLife)
                nowLife = maxLife;
        }
    }

    [Header("생명 주는 팝업")]
    public RectTransform panel;

    [Header("라이프 이미지 배열로 합시다")]
    private Image[] lifeImages = new Image[5];

    public Image[] LifeImages { get => lifeImages; set => lifeImages = value; }

    private StartOverManager startOverManager;
    private PeopleLineController peopleLineController;
    private TimeManager timeManager;

    private void Awake()
    {
        startOverManager = FindObjectOfType<StartOverManager>();
        peopleLineController = FindObjectOfType<PeopleLineController>();
        timeManager = FindObjectOfType<TimeManager>();

        timerChangeTimeWait = new WaitForSeconds(timerChangeTime);
    }

    // 초기 설정 해주는 함수
    private void StartSetting()
    {
        NowLife = startLife;

        for (int i = 0; i < lifeImagParent.childCount; i++)
        {
            LifeImages[i] = lifeImagParent.GetChild(i).GetComponent<Image>();
            if (i < NowLife)
                LifeImages[i].enabled = true;
            else LifeImages[i].enabled = false;
        }
    }

    private void WrongAnswer()
    {
        NowLife--;
        LifeImages[NowLife].enabled = false;
    }

    private void TimeOut()
    {
        NowLife--;
        LifeImages[NowLife].enabled = false;
        peopleLineController.SendMessage("Answer", false, SendMessageOptions.DontRequireReceiver);
        timeManager.SendMessage("TimerStop", SendMessageOptions.DontRequireReceiver);
        //timeManager.SendMessage("ResetTimer", SendMessageOptions.DontRequireReceiver);
    }

    private void PlusLife()
    {
        NowLife++;
        for (int i = 0; i < lifeImagParent.childCount; i++)
        {
            LifeImages[i] = lifeImagParent.GetChild(i).GetComponent<Image>();
            if (i < NowLife)
                LifeImages[i].enabled = true;
            else LifeImages[i].enabled = false;
        }
    }

    public void Reward(bool ads)
    {
        if (!ads)
            NowLife--;
        else
            StartCoroutine(_Reward());
        
        PopupManager.Instance.ClosedPopup(panel, true);
        
    }

    private IEnumerator _Reward()
    {
        moneyPanel.SetActive(false);
        WaitForSeconds oneTimeWait = new WaitForSeconds(1.0f);
        timeManager.SendMessage("TimerStop", SendMessageOptions.DontRequireReceiver);
        // 대충 4,3,2,1 타이머 나온다는 함수
        timerChangeTimeText.enabled = true;
        for (int i = 0; i < timerChangeTime; i++)
        {
            timerChangeTimeText.text = string.Format("{0}", timerChangeTime - i);
            yield return oneTimeWait;
        }

        timerChangeTimeText.enabled = false;
        moneyPanel.SetActive(true);
        timeManager.Pause(false);
        timeManager.SendMessage("ResetTimer", SendMessageOptions.DontRequireReceiver);
        yield break;
    }
}
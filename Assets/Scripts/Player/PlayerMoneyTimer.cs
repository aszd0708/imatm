﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using System.Threading;

/*
 * 2020-09-02
 * 
 * 한시간 지날때 마다 돈 충전 하는 스크립트
 * 1초마다 체크함
 * 만약 1시간이상 차이가 난다면 즉시 세이브파일에 있던 리워드 시간을 지금 시간으로 바꾼뒤
 * 다시 타이머를 돌림
 */

public class PlayerMoneyTimer : MonoBehaviour
{
    public UserDataSaveLoad userSaveData;
    public PlayerGiveMoneyController playerMoney;

    [Header("얼마 뒤에 충전될지")]
    public int rewardTime;

    [Header("시험용 텍스트")]
    public Text timeText;

    private void Start()
    {
        StartCoroutine(_TimerChecker());
    }

    private IEnumerator _TimerChecker()
    {
        WaitForSeconds wait = new WaitForSeconds(1.0f);
        while (true)
        {
            Check();
            yield return wait;
        }
    }

    private void Check()
    {
        DateTime nowTime = DateTime.Now;
        DateTime saveTime = DateTime.FromBinary(userSaveData.UserData.RewardTime);

        TimeSpan dif = nowTime - saveTime;

        timeText.text = string.Format("{0} : {1}", 59 - dif.Minutes, 59 - dif.Seconds);

        if (dif.Hours >= rewardTime) GetReward();
        else return;
    }

    private void GetReward()
    {
        playerMoney.AdReward();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerGiveMoneyController : AchievementObserver
{
    [Header("각 금액 남은 장수")]
    public Text count50000Text;

    public Text count10000Text, count5000Text, count1000Text;

    [Header("지금까지 낸 금액")]
    private int money;

    public int Money { get => money; set => money = value; }

    [Header("돈 텍스트")]
    public Text moneyText;

    private int moneyTouchCount;

    [Header("돈 세이브 파일")]
    public UserDataSaveLoad userData;

    [Header("VipCount알아보려고 넣음")]
    public PeopleCreater creater;

    public int MoneyTouchCount { get => moneyTouchCount; set => moneyTouchCount = value; }

    private PlayerController playerController;

    public GooglePlayGameManager googlePlayGameManager;

    private void Awake()
    {
        playerController = FindObjectOfType<PlayerController>();
    }

    private void Start()
    {
        SettingPaper();
        MoneyTouchCount = 0;
    }

    public void SettingPaper()
    {
        moneyText.text = string.Format("<color=#ffffff>\\ </color><color=#000000>{0}</color>", Money);

        count50000Text.text = string.Format("X {0}", userData.UserData.Money50000Count);
        count10000Text.text = string.Format("X {0}", userData.UserData.Money10000Count);
        count5000Text.text = string.Format("X {0}", userData.UserData.Money5000Count);
        count1000Text.text = string.Format("X {0}", userData.UserData.Money1000Count);

        SetPaperBtn();
    }

    private void SetPaperBtn()
    {
        int[] counts =
        {
            userData.UserData.Money50000Count,
            userData.UserData.Money10000Count,
            userData.UserData.Money5000Count,
            userData.UserData.Money1000Count
        };

        MoneyButtonController.Instance.SetPapersInGame(counts);
    }

    public bool GiveMoney(int value)
    {
        bool give;
        switch (value)
        {
            case 50000:
                if (userData.UserData.Money50000Count == 0)
                    give = false;
                else
                {
                    userData.UserData.Money50000Count--;
                    count50000Text.text = string.Format("X {0}", userData.UserData.Money50000Count);
                    give = true;
                }
                break;

            case 10000:
                if (userData.UserData.Money10000Count == 0)
                    give = false;
                else
                {
                    userData.UserData.Money10000Count--;
                    count10000Text.text = string.Format("X {0}", userData.UserData.Money10000Count);
                    give = true;
                }
                break;

            case 5000:
                if (userData.UserData.Money5000Count == 0)
                    give = false;
                else
                {
                    userData.UserData.Money5000Count--;
                    count5000Text.text = string.Format("X {0}", userData.UserData.Money5000Count);
                    give = true;
                }
                break;

            case 1000:
                if (userData.UserData.Money1000Count == 0)
                    give = false;
                else
                {
                    userData.UserData.Money1000Count--;
                    count1000Text.text = string.Format("X {0}", userData.UserData.Money1000Count);
                    give = true;
                }
                break;

            default:
                //Debug.LogError("잘못된 값을 집어넣었습니다.");
                give = false;
                break;
        }

        AudioManager.Instance.PlaySound(AudioManager.AudioKinds.COIN, Vector3.zero);

        Money += value;
        MoneyTouchCount++;

        SetPaperBtn();

        return give;
    }

    private bool CheckMoneyCount(int value)
    {
        bool give;

        switch (value)
        {
            case 50000:
                if (userData.UserData.Money50000Count <= 0)
                    give = false;
                else
                {
                    userData.UserData.Money50000Count--;
                    count50000Text.text = string.Format("X {0}", userData.UserData.Money50000Count);
                    give = true;
                }
                break;

            case 10000:
                if (userData.UserData.Money10000Count <= 0)
                    give = false;
                else
                {
                    userData.UserData.Money10000Count--;
                    count10000Text.text = string.Format("X {0}", userData.UserData.Money10000Count);
                    give = true;
                }
                break;

            case 5000:
                if (userData.UserData.Money5000Count <= 0)
                    give = false;
                else
                {
                    userData.UserData.Money5000Count--;
                    count5000Text.text = string.Format("X {0}", userData.UserData.Money5000Count);
                    give = true;
                }
                break;

            case 1000:
                if (userData.UserData.Money1000Count <= 0)
                    give = false;
                else
                {
                    userData.UserData.Money1000Count--;
                    count1000Text.text = string.Format("X {0}", userData.UserData.Money1000Count);
                    give = true;
                }
                break;

            default:
                //Debug.LogError("잘못된 값을 집어넣었습니다.");
                give = false;
                break;
        }

        SetPaperBtn();

        return give;
    }

    public void GiveMoneyNoBtn(int value)
    {
        bool give = CheckMoneyCount(value);

        if (give)
        {
            Money += value;
            AudioManager.Instance.PlaySound(AudioManager.AudioKinds.COIN, Vector3.zero);
        }

        MoneyTouchCount = 1;
        playerController.SendMessage("ShowMoney", Money, SendMessageOptions.DontRequireReceiver);

        if (playerController.PersonState == null)
            return;

        if (playerController.PersonState.money <= Money)
        {
            playerController.SendMessage("CheckingMoney", SendMessageOptions.DontRequireReceiver);
        }
    }

    public void ResetValues()
    {
        Money = 0;
        MoneyTouchCount = 0;
        moneyText.text = string.Format("<color=#ffffff>\\ </color><color=#000000>{0}</color>", Money);
    }

    private void SaveUserData()
    {
        //Debug.Log(this.name + "저장됨");
        userData.SaveData();
    }

    public void SetAdRewardToMax()
    {
        userData.UserData.RewardToMax();
        SettingPaper();
        userData.SaveData();
        // 일단 어캐할지 몰라서 넣음
        GPGSSubject.Instance.Notify();
    }

    public void AdReward()
    {
        userData.UserData.RewardAdd();
        SettingPaper();
        userData.SaveData();
        // 일단 어캐할지 몰라서 넣음
        GPGSSubject.Instance.Notify();
    }

    public override void GetAcheivement()
    {
        int count = userData.UserData.MaxMoney50000Count;

        if (count >= 200 && count < 300)
        {
            //Debug.Log("도전과제 달성");
            googlePlayGameManager.GetAchievement(GPGSIds.achievementMAXMoney200);
        }

        else if (count >= 300 && count < 400)
        {
            //Debug.Log("도전과제 달성");
            googlePlayGameManager.GetAchievement(GPGSIds.achievementMAXMoney300);
        }

        else if (count >= 400 && count < 500)
        {
            //Debug.Log("도전과제 달성");
            googlePlayGameManager.GetAchievement(GPGSIds.achievementMAXMoney400);
        }
        else if (count >= 500 && count < 600)
        {
            //Debug.Log("도전과제 달성");
            googlePlayGameManager.GetAchievement(GPGSIds.achievementMAXMoney500);
        }
        else if (count >= 600 && count < 700)
        {
            //Debug.Log("도전과제 달성");
            googlePlayGameManager.GetAchievement(GPGSIds.achievementMAXMoney600);
        }
        else if (count >= 700 && count < 800)
        {
            //Debug.Log("도전과제 달성");
            googlePlayGameManager.GetAchievement(GPGSIds.achievementMAXMoney700);
        }
        else if (count >= 800 && count < 900)
        {
            //Debug.Log("도전과제 달성");
            googlePlayGameManager.GetAchievement(GPGSIds.achievementMAXMoney800);
        }
        else if (count >= 900 && count < 1000)
        {
            //Debug.Log("도전과제 달성");
            googlePlayGameManager.GetAchievement(GPGSIds.achievementMAXMoney900);
        }

        else if (count >= 1000)
        {
            //Debug.Log("도전과제 달성");
            googlePlayGameManager.GetAchievement(GPGSIds.achievementWereGonnaBeRich);
            GPGSSubject.Instance.RemoveObserver(this);
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveLevelController : WaveLevelSettingManager
{
    [Header("최대 웨이브 레벨")]
    public int maxLevel;

    private int nowLevel = 1;
    public int NowLevel
    {
        get => nowLevel;
        set
        {
            if (value > maxLevel-1)
                value = maxLevel-1;
            nowLevel = value;
        }
    }

    // 바꿔야댐 해결 X 나온 O
    [Header("지금까지 해결한 사람들의 수")]
    private int peopleCount = 0;
    public int PeopleCount
    {
        get => peopleCount;
        set
        {
            peopleCount = value;
            if (peopleCount > WaveLevelInfos[NowLevel].PersonCountList[1])
                NowLevel++;
        }
    }

    [Header("유저 데타")]
    public UserDataSaveLoad userData;


    private void Awake()
    {
        SetInfos();
        BasicSetting();

        maxLevel = System.Convert.ToInt32(WaveLevelInfos[WaveLevelInfos.Count - 1].WaveLevel);
    }

    public void Start()
    {
        PeopleCount = 0;
    }

    public List<int> GetTouchCount()
    {
        return WaveLevelInfos[NowLevel - 1].TouchCountList;
    }

    public List<int> GetPercent()
    {
        return WaveLevelInfos[NowLevel - 1].PercentList;
    }

    private void BasicSetting()
    {
        if (userData.UserData.SelectFirstVipPerson + 1 < 1)
            NowLevel = 1;
        else NowLevel = (userData.UserData.SelectFirstVipPerson + 1) * 3 + 1;

        PeopleCount = WaveLevelInfos[NowLevel-1].PersonCountList[0];
    }
}

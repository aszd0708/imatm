﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GameManager : Singleton<GameManager>
{
    private bool isGameOver;
    public bool IsGameOver { get => isGameOver; set => isGameOver = value; }

    private void Start()
    {
        IsGameOver = true;
    }
}

﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VipListPositionSetting : MonoBehaviour
{
    public RectTransform rect;
    private bool isFirst = true;

    public void OnDisable()
    {
        if (isFirst) { SetTransform(); isFirst = false; }
    }

    public void SetTransform()
    {
       StartCoroutine(_SetTransform());
    }

    private IEnumerator _SetTransform()
    {
        rect.localPosition = Vector3.zero;
        yield break;
    }
}

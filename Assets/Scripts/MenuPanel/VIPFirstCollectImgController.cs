﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VIPFirstCollectImgController : MonoBehaviour
{
    public GameObject newMark;

    public void SetNewMark(bool isNew)
    {
        if (isNew)
            newMark.SetActive(true);
        else newMark.SetActive(false);
    }
}

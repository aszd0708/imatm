﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public interface IMenuPanel
{
    void DoOn();
    void DoOff();
}

public class MenuPanel : MonoBehaviour, IMenuPanel
{
    public UnityEvent doEvent, offEvent;

    public void DoOn()
    {
        doEvent.Invoke();
    }

    public void DoOff()
    {
        offEvent.Invoke();
    }
}

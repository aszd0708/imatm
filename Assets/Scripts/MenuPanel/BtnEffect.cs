﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BtnEffect : MonoBehaviour
{
    public void ClickBTN()
    {
        AudioManager.Instance.PlaySound(AudioManager.AudioKinds.BTN, Vector3.zero);
    }
}

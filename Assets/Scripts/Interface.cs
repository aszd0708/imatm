﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PersonStateEnum
{
    ANGER, HAPPY, NORMAL
};

interface IPerson
{
    /// <summary>
    /// 가장 앞에 있는 사람의 정보를 넘기는 함수
    /// </summary>
    void SendPersonInfo();
    /// <summary>
    /// 돈을 랜덤으로 정하는 함수
    /// 범위 지정되면 그때 값을 넣어 사용
    /// </summary>
    void RandomMoney();
    /// <summary>
    /// 생김새를 랜덤으로 정하는 함수
    /// </summary>
    void RandomVisual();
    /// <summary>
    /// 텍스트에 돈을 쓰는 함수
    /// </summary>
    void SetPriceText();
}

interface IKindOfPerson
{
    /// <summary>
    /// 돈을 받고 나오는 행동
    /// </summary>
    void Answer(PersonStateEnum state);
    /// <summary>
    /// 풀링할때 이름
    /// </summary>
    string PoolingName { get; set; }
}

interface IDoorAnimation
{
    /// <summary>
    /// 문이 열리는 애니메이션
    /// </summary>
    void OpenAnimation();
    /// <summary>
    /// 문이 닫히는 애니메이션
    /// </summary>
    void ClosedAnimation();
}

interface IVipScrollList
{
    /// <summary>
    /// 각 요소에 어떤 변수 및 함수를 넣을지 정해서 넣는 함수
    /// </summary>
    /// <param name="vipSprite"></param>
    /// <param name="faceSprite"></param>
    /// <param name="OpenScore"></param>
    void InfoSetting(int _index, Sprite vipSprite, Sprite faceSprite, string OpenScore, VIPPersonScorllListCreater _VipListSetting);
    /// <summary>
    /// 수집했는지 확인하는 함수
    /// </summary>
    /// <param name="isCollect"></param>
    void IsCollect(bool isCollect);

    /// <summary>
    /// 처음 수집했을때 N마크 넣음
    /// </summary>
    /// <param name="isFirstCollect"></param>
    void FirstCollcet(bool isFirstCollect);
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Intro : MonoBehaviour
{
    public RectTransform ATM;

    public Image man;

    public Image[] effect;

    public Image[] allImgs;

    public Color fadeInColor, fadeOutColor;

    public Image background;

    public AudioSource intro_1, intro_2;

    public Image ATMImg;


    public static string nextScene;

    public bool isIntro;
    private void Start()
    {
        if (!isIntro)
            StartCoroutine(_EndIntro());
    }

    private IEnumerator _EndIntro()
    {
        for (int i = 0; i < allImgs.Length; i++)
            allImgs[i].DOFade(0.0f, 0.5f);
        yield return new WaitForSeconds(0.5f);
        background.DOFade(0.0f, 0.25f);
        yield return new WaitForSeconds(0.25f);
        gameObject.SetActive(false);
        yield break;
    }

    public void PlayIntro()
    {
        StartCoroutine(_PlayIntro());
    }

    private IEnumerator _PlayIntro()
    {
        float backVol = PlayerPrefs.GetFloat("backVol", 1.0f);
        yield return new WaitForSeconds(0.25f);
        //AudioManager.Instance.PlaySound("intro_1", Vector2.zero);
        intro_1.volume = backVol;
        intro_1.Play();
        ATM.DOAnchorPosY(0, 1f).SetEase(Ease.Linear);
        yield return new WaitForSeconds(0.9f);
        for (int i = 0; i < effect.Length; i++)
            effect[i].enabled = true;
        //AudioManager.Instance.PlaySound("intro_2", Vector2.zero);
        intro_2.volume = backVol;
        intro_2.Play();
        ATM.DOShakeAnchorPos(0.5f, 100.0f, 50, 45f);
        yield return new WaitForSeconds(0.2f);
        for (int i = 0; i < effect.Length; i++)
            effect[i].enabled = false;
        yield return new WaitForSeconds(0.3f);
        man.DOFade(0.76f, 1f);
        yield return new WaitForSeconds(2.0f);
        for (int i = 0; i < allImgs.Length; i++)
            allImgs[i].DOFade(0.0f, 0.5f);
        LoadScene("MainScene");
        yield return StartCoroutine(LoadScene());
        background.DOFade(0.0f, 0.5f);
        yield return new WaitForSeconds(0.5f);
        //gameObject.SetActive(false);
        yield break;
    }
    public static void LoadScene(string sceneName)
    {
        nextScene = sceneName;
        SceneManager.LoadScene(sceneName);
    }

    IEnumerator LoadScene()
    {
        yield return null;
        AsyncOperation op = SceneManager.LoadSceneAsync(nextScene);
        op.allowSceneActivation = false;

        float timer = 0.0f;
        yield return new WaitUntil(() => op.progress >= 0.9f);
        op.allowSceneActivation = true;
        Debug.Log("끝? : " + op.isDone);
        yield break;
    }
}

﻿using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using UnityEngine;

public class GoogleCloudDataManager : SerializableManager<byte[]>
{
    [Header("세이브 데이터 이름")]
    public string dataName;
    private string data;
    public string Data 
    {
        get
        {
            return data;
        }
        set
        {
            data = value;
        }
    }

    private void Start()
    {
        SaveDataName = dataName;
    }

    public byte[] GetData()
    {
        return GetDataToByte();
    }
}

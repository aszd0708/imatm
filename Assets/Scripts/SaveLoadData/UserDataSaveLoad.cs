﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2020-06-01
 * 각각 돈의 갯수를 세이브 및 로드 해주는 함수
 * 직렬화를 사용함
 * 기본적으로 게임을 하다가 게임 오버가 되면 남은 값들을 저장,
 * 광고를 보고 최대치가 올라갔을 경우 저장
 * 게임을 켰을때 로드
 */

[System.Serializable]
public class UserData
{
    private int money50000Count;
    private int money10000Count;
    private int money5000Count;
    private int money1000Count;

    private int maxMoney50000Count;
    private int maxMoney10000Count;
    private int maxMoney5000Count;
    private int maxMoney1000Count;

    private int vipPersonCollect;

    private int selectFirstVipPerson;

    private int maximumScore;

    private long rewardTime;

    public int Money50000Count { get => money50000Count; set => money50000Count = value; }
    public int Money10000Count { get => money10000Count; set => money10000Count = value; }
    public int Money5000Count { get => money5000Count; set => money5000Count = value; }
    public int Money1000Count { get => money1000Count; set => money1000Count = value; }
    public int MaxMoney50000Count 
    { 
        get => maxMoney50000Count;
        set
        {
            if (value >= 999999)
                value = 999999;
            maxMoney50000Count = value;
        }
    }
    public int MaxMoney10000Count 
    { 
        get => maxMoney10000Count;
        set
        {
            if (value >= 999999)
                value = 999999;
            maxMoney10000Count = value;
        }
    }
    public int MaxMoney5000Count
    {
        get => maxMoney5000Count;
        set
        {
            if (value >= 999999)
                value = 999999;
            maxMoney5000Count = value;
        }
    }
    public int MaxMoney1000Count 
    { 
        get => maxMoney1000Count;
        set
        {
            if (value >= 999999)
                value = 999999;
            maxMoney1000Count = value;
        }
    }
    public int VipPersonCollect { get => vipPersonCollect; set => vipPersonCollect = value; }
    public int SelectFirstVipPerson { get => selectFirstVipPerson; set => selectFirstVipPerson = value; }
    public int MaximumScore { get => maximumScore; set => maximumScore = value; }
    public long RewardTime { get => rewardTime; set => rewardTime = value; }

    public void PrintMoneyData()
    {
        //Debug.Log("50000원 : " + Money50000Count + "장");
        //Debug.Log("10000원 : " + Money10000Count + "장");
        //Debug.Log("5000원 : " + Money5000Count + "장");
        //Debug.Log("1000원 : " + Money1000Count + "장");
    }

    public void RewardAdd()
    {
        MaxMoney50000Count+=10;
        Money50000Count = MaxMoney50000Count;
        MaxMoney10000Count += 10;
        Money10000Count = MaxMoney10000Count;
        MaxMoney5000Count += 10;
        Money5000Count = MaxMoney5000Count;
        MaxMoney1000Count += 10;
        Money1000Count = MaxMoney1000Count;

        RewardTime = System.DateTime.Now.Ticks;

        int[] counts =
        {
            Money50000Count,
            Money10000Count,
            MaxMoney5000Count,
            MaxMoney1000Count
        };

        MoneyButtonController.Instance.SetPapers(counts);
    }

    public void RewardToMax()
    {
        Money50000Count = MaxMoney50000Count;
        Money10000Count = MaxMoney10000Count;
        Money5000Count = MaxMoney5000Count;
        Money1000Count = MaxMoney1000Count;

        RewardTime = System.DateTime.Now.Ticks;

        int[] counts =
        {
            Money50000Count,
            Money10000Count,
            MaxMoney5000Count,
            MaxMoney1000Count
        };

        MoneyButtonController.Instance.SetPapers(counts);
    }

    public void Setting()
    {
        Money50000Count = MaxMoney50000Count;
        Money10000Count = MaxMoney10000Count;
        Money5000Count = MaxMoney5000Count;
        Money1000Count = MaxMoney1000Count;
    }
}

public class UserDataSaveLoad : SerializableManager<UserData>
{
    [Header("세이브 데이터 이름")]
    public string dataName;

    [Header("초기 데이터")]
    public int first50000Data;

    public int first10000Data, first5000Data, first1000Data;
    

    [Header("데이터 변수")]
    [SerializeField]
    private UserData userData = new UserData();

    public UserData UserData { get => userData; set => userData = value; }

    private void Awake()
    {
        SetFileName(dataName);
        SetData(ref userData);
    }

    /// <summary>
    /// 빌드하기 전에 지우기
    /// </summary>
    /// <param name="data"></param>
    public void DeveloperSetData(ref UserData data)
    {
        data = LoadData<UserData>();

        if (data == null)
        {
            data = new UserData
            {
                MaxMoney50000Count = 10000,
                MaxMoney10000Count = 10000,
                MaxMoney5000Count = 10000,
                MaxMoney1000Count = 10000,
                VipPersonCollect = 9,
                SelectFirstVipPerson = -1,
                MaximumScore = 0,
                RewardTime = System.DateTime.Now.Ticks
            };

            data.Setting();
            SaveData(data);
        }
    }

    public void SetData(ref UserData data)
    {
        data = LoadData<UserData>();

        if (data == null)
        {
            data = new UserData
            {
                MaxMoney50000Count = first50000Data,
                MaxMoney10000Count = first10000Data,
                MaxMoney5000Count = first5000Data,
                MaxMoney1000Count = first1000Data,
                VipPersonCollect = 0,
                SelectFirstVipPerson = -1,
                MaximumScore = 0,
                RewardTime = System.DateTime.Now.Ticks
            };

            data.Setting();
            SaveData(data);
        }
    }

    public void SaveData(ref UserData data)
    {
        SaveData(data);
    }

    public void SaveData()
    {
        SaveData(UserData);
    }
}
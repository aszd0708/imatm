﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class MainRewardEffect : MonoBehaviour
{
    public float textMovePoseY = 300f;
    private Vector2 textFirstPose;
    public GameObject coin;
    public Text maxText;
    public RectTransform textRect;

    public int createCoinCount = 15;

    public float effectTime;

    private Coroutine effectCor;

    private void Start()
    {
        textFirstPose = textRect.anchoredPosition;
        effectCor = null;
        maxText.enabled = false;
    }

    public void PlayerEffect()
    {
        if (effectCor != null)
            StopCoroutine(effectCor);
        effectCor = StartCoroutine(_PlayEffect());
    }

    private IEnumerator _PlayEffect()
    {
        textRect.anchoredPosition = textFirstPose;
        maxText.color = Color.white;
        maxText.enabled = true;
        GameObject createCoin;
        for(int i = 0; i < createCoinCount; i++)
        {
            createCoin = PoolingManager.Instance.GetPool("ParticleObj");
            if(createCoin == null)
                createCoin = Instantiate(coin);
        }
        maxText.GetComponent<RectTransform>().DOAnchorPosY(textMovePoseY, effectTime);
        maxText.DOFade(0, effectTime);
        AudioManager.Instance.PlaySound(AudioManager.AudioKinds.PAY, Vector2.zero);
        yield return new WaitForSeconds(effectTime);
        effectCor = null;
        maxText.enabled = false;
        yield break;
    }
}

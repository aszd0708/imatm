﻿using GoogleMobileAds.Api;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2020-06-02
 * 광고보고 생명 하나 더 주는 대충 그런 클래스
 */

public class LifeRewardAds : GoogleAdManager
{
    [Header("플레이어 라이프 컨트롤러")]
    public PlayerLifeController lifeController;

    [Header("플레이어 돈 컨트롤러")]
    public PlayerGiveMoneyController moneyController;

    private bool showAd = false;

    private void Start()
    {
        RequestInterstitial();

        StartCoroutine(GetReward());
    }

    private void RequestInterstitial()
    {
#if UNITY_ANDROID
        string adUnitId = androidAdUnitID;
#elif UNITY_IPHONE
        string adUnitId = iphoneAdUnitID;
#else
        string adUnitId = "unexpected_platform";
#endif

        rewardedAd = new RewardedAd(adUnitId);
        this.rewardedAd.OnAdLoaded += HandleRewardedAdLoaded;
        // Called when an ad request failed to load.
        this.rewardedAd.OnAdFailedToLoad += HandleRewardedAdFailedToLoad;
        // Called when an ad is shown.
        this.rewardedAd.OnAdOpening += HandleRewardedAdOpening;
        // Called when an ad request failed to show.
        this.rewardedAd.OnAdFailedToShow += HandleRewardedAdFailedToShow;
        // Called when the user should be rewarded for interacting with the ad.
        this.rewardedAd.OnUserEarnedReward += HandleUserEarnedReward;
        // Called when the ad is closed.
        this.rewardedAd.OnAdClosed += HandleRewardedAdClosed;

        AdRequest request = new AdRequest.Builder()
            .AddTestDevice(AdRequest.TestDeviceSimulator).AddTestDevice("92363741D9B756D79935C13DA2ADDDB0").
        Build();
        rewardedAd.LoadAd(request);
    }

    public void HandleRewardedAdLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardedAdLoaded event received");
    }

    public void HandleRewardedAdFailedToLoad(object sender, AdErrorEventArgs args)
    {
        MonoBehaviour.print(
            "HandleRewardedAdFailedToLoad event received with message: "
                             + args.Message);
    }

    public void HandleRewardedAdOpening(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardedAdOpening event received");
    }

    public void HandleRewardedAdFailedToShow(object sender, AdErrorEventArgs args)
    {
        MonoBehaviour.print(
            "HandleRewardedAdFailedToShow event received with message: "
                             + args.Message);
    }

    public void HandleRewardedAdClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardedAdClosed event received");
        RequestInterstitial();
    }

    public void HandleUserEarnedReward(object sender, Reward args)
    {
        string type = args.Type;
        double amount = args.Amount;
        MonoBehaviour.print(
            "HandleRewardedAdRewarded event received for "
                        + amount.ToString() + " " + type);

        showAd = true;
        //RequestInterstitial();
    }

    public void HandleOnAdLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdLoaded event received");
    }

    public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print("광고 실패 이유 : "
                            + args.Message);
    }

    public void HandleOnAdOpened(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdOpened event received");
    }

    public void HandleOnAdClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdClosed event received");
        // RequestInterstitial();
    }

    public void HandleOnAdLeavingApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdLeavingApplication event received");
    }

    public void AdsShow()
    {
        if (this.rewardedAd.IsLoaded())
        {
            this.rewardedAd.Show();
            print("광고 시작");
            //RequestInterstitial();
            //PGMC.SendMessage("AdReward", SendMessageOptions.DontRequireReceiver);
        }
        else
        {
            //Debug.Log("로드 안댐");
            RequestInterstitial();
        }
    }

    private IEnumerator GetReward()
    {
        WaitUntil wait = new WaitUntil(() => showAd);
        while (true)
        {
            yield return wait;
            showAd = false;

            lifeController.Reward(true);
            moneyController.AdReward();
           // Debug.Log("광고 보상 드루옴");
        }
    }
}
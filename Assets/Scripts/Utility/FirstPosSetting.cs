﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPosSetting : MonoBehaviour
{
    private Vector3 m_leftDown;
    
    void Awake()
    {
        float scaleValue = SettingScale(GetFactor());
        m_leftDown = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0));
        transform.position = new Vector3(m_leftDown.x, m_leftDown.y, 0);
        transform.localScale = new Vector3(scaleValue, scaleValue, 0);
    }

    private float GetFactor()
    {
        float width = Screen.width, height = Screen.height;
        float max = 0, min = 0;
        if (width > height)
        {
            max = height;
            min = width;
        }
        else
        {
            max = width;
            min = height;
        }
        while(max % min != 0)
        {
            float temp = max % min;
            max = min;
            min = temp;
        }

        return min;
    }

    private float SettingScale(float factor)
    {
        float width = Screen.width / factor;
        float height = Screen.height / factor;
        float value = ((width / height) / (16f / 9f)) * 0.75f;

        return value;
    }
}

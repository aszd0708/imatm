﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntToBool : MonoBehaviour
{
    public static int GetInt(bool value)
    {
        if (value)
            return 1;
        else return 0;
    }

    public static bool GetBool(int value)
    {
        if (value == 0) return false;
        else return true;
    }
}

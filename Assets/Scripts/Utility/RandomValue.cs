﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomValue : MonoBehaviour
{
    static public T GetRandomValue<T>(T[] values, float[] percent)
    {
        T randomValue = default;
        float random = Random.Range(0f, 100f);
        float percentIndex = 0;

        for (int i = 0; i < percent.Length; i++)
        {
            percentIndex += percent[i];
            if (random <= percentIndex)
                randomValue = values[i];
        }

        return randomValue;
    }


    static public T GetRandomValue<T>(T[] values, int[] percent)
    {
        T randomValue = default;
        int random = Random.Range(0, 10000);

        int percentIndex = 0;
        for (int i = 0; i < percent.Length; i++)
        {
            percentIndex += percent[i];
            if (random <= percentIndex)
                randomValue = values[i];
        }

        return randomValue;
    }
    static public T GetRandomValue<T>(T[] values, List<int> percent)
    {
        T randomValue = default;
        int random = Random.Range(0, 10000);

        int percentIndex = 0;
        for (int i = 0; i < percent.Count; i++)
        {
            percentIndex += percent[i];
            if (random <= percentIndex)
                randomValue = values[i];
        }

        return randomValue;
    }

    static public T GetRandomValue<T>(List<T> values, int[] percent)
    {
        T randomValue = default;
        int random = Random.Range(0, 10000);

        int percentIndex = 0;
        for (int i = 0; i < percent.Length; i++)
        {
            percentIndex += percent[i];
            if (random <= percentIndex)
                randomValue = values[i];
        }

        return randomValue;
    }
    static public T GetRandomValue<T>(List<T> values, List<int> percent)
    {
        T randomValue = default;
        int random = Random.Range(0, 10000);
        int percentIndex = 0;
        for (int i = 0; i < percent.Count; i++)
        {
            percentIndex += percent[i];
            if (random <= percentIndex)
                randomValue = values[i];
        }

        return randomValue;
    }

    static public int GetRandomValue(float[] percent)
    {
        int randomValue = 0;
        float random = Random.Range(0f, 100f);
        float percentIndex = 0;

        for (int i = 0; i < percent.Length; i++)
        {
            percentIndex += percent[i];
            if (random <= percentIndex)
                randomValue = i;
        }

        return randomValue;
    }
}

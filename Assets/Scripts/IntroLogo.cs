﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class IntroLogo : MonoBehaviour
{
    public Image backgroundImg;
    public Image logoImg;
    public RectTransform logoRect;

    public GameObject obj;

    public Intro intro;

    private void Start()
    {
        StartCoroutine(_PlayIntro());
    }

    private IEnumerator _PlayIntro()
    {
        yield return new WaitForSeconds(0.5f);
        logoRect.DOScale(Vector3.one, 1.0f).SetEase(Ease.OutElastic);
        yield return new WaitForSeconds(2.0f);
        backgroundImg.DOFade(0, 0.75f);
        logoImg.DOFade(0, 0.5f);
        yield return new WaitForSeconds(1.0f);
        obj.SetActive(false);
        intro.PlayIntro();
        yield break;
    }
}

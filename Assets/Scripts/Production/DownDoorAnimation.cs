﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DownDoorAnimation : MonoBehaviour, IDoorAnimation
{
    [Header("내려올 문")]
    public RectTransform door;

    [Header("움직이는 시간")]
    public float animateTime;

    [Header("Y축 들")]
    private  float openY = 2100f;

    private float closedY = 0.0f;   

    [Header("카메라")]
    public Transform cam;

    private WaitForSeconds waitTime;

    public RectTransform canvasRect;

    private RectTransform rect;

    private void Awake()
    {
        rect = GetComponent<RectTransform>();
    }

    private void Start()
    {
        waitTime = new WaitForSeconds(animateTime);
        CalDownPose();
    }

    private void CalDownPose()
    {
        float gap = Mathf.Abs(1920 - canvasRect.sizeDelta.y);
        closedY -= gap;
        door.anchoredPosition = new Vector2(door.anchoredPosition.x, closedY);
    }

    public void OpenAnimation()
    {
        StartCoroutine(_OpenAnimation());
    }

    private IEnumerator _OpenAnimation()
    {
        door.DOAnchorPosY(openY, animateTime + animateTime).SetEase(Ease.InBack);
        yield return waitTime;
        yield return waitTime;
        yield break;
    }

    public void ClosedAnimation()
    {
        StartCoroutine(_ClosedAnimation());
    }

    //private IEnumerator _ClosedAnimation()
    //{
    //    door.DOLocalMoveY(closedY, animateTime).SetEase(Ease.Linear);
    //    yield return waitTime;
    //    AudioManager.Instance.PlaySound(AudioManager.AudioKinds.OVER, Vector3.zero);
    //    //cam.DOShakePosition(0.5f, 0.5f, 20, 45);
    //    GetComponent<RectTransform>().DOShakeAnchorPos(0.5f, 100, 20, 45);
    //    yield break;
    //}

    private IEnumerator _ClosedAnimation()
    {
        door.DOAnchorPosY(closedY, animateTime).SetEase(Ease.Linear);
        yield return waitTime;
        AudioManager.Instance.PlaySound(AudioManager.AudioKinds.OVER, Vector3.zero);
        //cam.DOShakePosition(0.5f, 0.5f, 20, 45);
        rect.DOShakeAnchorPos(0.5f, 100, 20, 45);
        yield break;
    }
}
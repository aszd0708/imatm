﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoldingDoorAnimation : MonoBehaviour, IDoorAnimation
{
    [Header("각 판의 크기")]
    public float size;
    [Header("뒤집어지는 속도")]
    public float speed;

    private List<RectTransform> panel = new List<RectTransform>();

    private void SetPanels()
    {
        for (int i = 0; i < transform.childCount; i++)
            panel.Add(transform.GetChild(i).GetComponent<RectTransform>()) ;
    }

    private void SetPanelsReverse()
    {
        for (int i = transform.childCount - 1; i >= 0; i--)
            panel.Add(transform.GetChild(i).GetComponent<RectTransform>());
    }

    private void Start()
    {
        SetPanels();
    }

    public void ClosedAnimation()
    {
        StartCoroutine(_ClosedAnimation());
    }

    private IEnumerator _ClosedAnimation()
    {
        WaitForSeconds wait = new WaitForSeconds(speed);
        Vector3 rot = new Vector3(180, 0, 0);

        int panelCount = panel.Count;
        for(int i = 0; i < panelCount; i++)
        {
            panel[0].DORotate(rot, speed).SetEase(Ease.Linear);
            panel.RemoveAt(0);
            yield return wait;
            //for (int a = 0; a < panel.Count; a++)
            if(i != panelCount - 1)
                panel[0].localPosition = new Vector3(0, panel[0].localPosition.y - size * (i + 1), 0);
        }
        SetPanelsReverse();
        yield break;
    }

    public void OpenAnimation()
    {
        StartCoroutine(_OpenAnimation());
    }

    private IEnumerator _OpenAnimation()
    {
        WaitForSeconds wait = new WaitForSeconds(speed);
        
        int panelCount = panel.Count;
        for (int i = 0; i < panelCount; i++)
        {
            panel[i].DOLocalRotateQuaternion(Quaternion.identity, speed).SetEase(Ease.Linear);
            yield return wait;
            yield return wait;
            panel[i].localPosition = Vector3.zero;
        }
        SetPanels();
        yield break;
    }
}

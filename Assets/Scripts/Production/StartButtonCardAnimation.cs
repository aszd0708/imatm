﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class StartButtonCardAnimation : MonoBehaviour
{
    [Header("총 움직이는 시간")]
    public float moveTime = 1.0f;

    public RectTransform rect;

    public Button btn;

    private float movePoseX = -1100f;

    private WaitForSeconds waitSecond;

    private void Start()
    {
        waitSecond = new WaitForSeconds(moveTime);
    }

    public void Animation(bool isInsert)
    {
        if(isInsert)
            StartCoroutine(_InAnimation());

        else
            StartCoroutine(_OutAnimation());
    }
    private IEnumerator _InAnimation()
    {
        rect.DOAnchorPosX(movePoseX, moveTime);
        btn.enabled = false;
        yield return waitSecond;
        yield break;
    }

    private IEnumerator _OutAnimation()
    {
        rect.DOAnchorPosX(0, moveTime);
        yield return waitSecond;
        btn.enabled = true;
        yield break;
    }
}

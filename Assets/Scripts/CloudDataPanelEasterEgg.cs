﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudDataPanelEasterEgg : MonoBehaviour
{
    public int touchCount;
    private int nowTouchCount;

    public RectTransform dataRect;

    private void Start()
    {
        NowTouchCount = 0;
    }

    public int NowTouchCount 
    { 
        get => nowTouchCount;
        set
        {
            nowTouchCount = value;
            if(nowTouchCount == touchCount)
            {
                PopupManager.Instance.OpenPopup(dataRect, false);
                NowTouchCount = 0;
            }
        }
    }

    public void Touch()
    {
        NowTouchCount++;
    }
}

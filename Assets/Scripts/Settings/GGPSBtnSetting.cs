﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GGPSBtnSetting : MonoBehaviour
{
    [Header("구글 게임플레이 매니져")]
    public GooglePlayGameManager GGPSM;

    [Header("아이디 나오는 텍스트")]
    public Text loginText;

    [Header("로그인 버튼")]
    public Button loginBtn;
    [Header("로그아웃 버튼")]
    public Button logoutBtn;

    public GameObject logInObj, logOutObj;

    public void Logout()
    {
        logoutBtn.enabled = false;
        loginBtn.enabled = true;

        LogBtnSetting(true);
    }

    public void Login()
    {
        logoutBtn.enabled = true;
        loginBtn.enabled = false;

        LogBtnSetting(false);
    }

    private void LogBtnSetting(bool isLogIn)
    {
        logInObj.SetActive(isLogIn);
        logOutObj.SetActive(isLogIn);
    }
}

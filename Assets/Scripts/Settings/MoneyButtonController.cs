﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyButtonController : Singleton<MoneyButtonController>
{
    public MoneyButtonSetting[] papers;

    /// <summary>
    /// 카운트 50000 -> 10000 -> 5000 -> 1000 순으로
    /// </summary>
    /// <param name="counts"></param>
    public void SetPapersInGame(int[] counts)
    {
        if (counts.Length != 4)
        {
            //Debug.Log("값 잘못 입력함");
            return;
        }

        for (int i = 0; i < counts.Length; i++)
        {
            papers[i].SetPaper(counts[i]);
        }
    }

    public void SetPapers(int[] counts)
    {
        if (counts.Length != 4)
        {
            //Debug.Log("값 잘못 입력함");
            return;
        }

        for (int i = 0; i < counts.Length; i++)
        {
            papers[i].SetPaper(counts[i]);
        }
    }
}

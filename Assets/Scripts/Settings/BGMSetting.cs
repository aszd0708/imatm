﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BGMSetting : MonoBehaviour
{
    public Slider backVolume;
    public Image soundImage;
    public Sprite[] soundSprite = new Sprite[2];
    private float backVol = 1f;

    public new AudioSource audio;
    public AudioSource feverAudio;

    private void Start()
    {
        backVol = PlayerPrefs.GetFloat("backVol", 1.0f);
        backVolume.value = backVol;
        feverAudio.volume = backVolume.value;
        audio.volume = backVolume.value;
    }

    public void SoundSlider()
    {
        if (backVolume.value <= 0)
            soundImage.sprite = soundSprite[1];
        else
            soundImage.sprite = soundSprite[0];
        audio.volume = backVolume.value;
        feverAudio.volume = backVolume.value;
        backVol = backVolume.value;
        PlayerPrefs.SetFloat("backVol", backVol);
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class MoneyButtonSetting : MonoBehaviour
{
    private Button moneyBtn;

    private Color yesMoneyNormalColor;
    private Color yesMoneyTouchColor;
    public Color noMoneyNormalColor;
    public Color noMoneyTouchColor;

    private ColorBlock yesMoneyColorblock;
    private ColorBlock noMoneyColorBlock;

    private RectTransform rect;

    private bool noMoney;

    public bool NoMoney { get => noMoney; set => noMoney = value; }

    [Header("돈 없을때 애니메이션 시간")]
    public float noTime = 0.25f;

    public float power;
    public int vib;

    private WaitForSeconds noWait;

    private void Awake()
    {
        moneyBtn = GetComponent<Button>();
        rect = GetComponent<RectTransform>();
    }

    private void Start()
    {
        yesMoneyColorblock = moneyBtn.colors;

        noMoneyColorBlock = moneyBtn.colors;

        noMoneyColorBlock.normalColor = noMoneyNormalColor;
        noMoneyColorBlock.pressedColor = noMoneyTouchColor;
        noMoneyColorBlock.highlightedColor = noMoneyNormalColor;

        noWait = new WaitForSeconds(noTime);
    }

    public void SetPaper(int moneyCount)
    {
        SetColor(moneyCount);
        SetAnimation(moneyCount);
    }

    public void SetColor(int moneyCount)
    {
        if (moneyCount <= 0)
        {
            // 여기 적을때
            moneyBtn.colors = noMoneyColorBlock;
        }

        else
        {
            // 여기 많을때
            moneyBtn.colors = yesMoneyColorblock;
        }
    }

    public void SetAnimation(int moneyCount)
    {
        if (moneyCount <= 0)
            NoMoney = true;
        else NoMoney = false;
    }

    public void PlayAnimation()
    {
        if (NoMoney)
        {
            // 여기 돈 없을때 애니메이션
            StartCoroutine(_NoMoneyAnimation());
            AudioManager.Instance.PlaySound(AudioManager.AudioKinds.EMPTYMONEY, Vector3.zero);
        }
        else return;
    }

    private IEnumerator _NoMoneyAnimation()
    {
        moneyBtn.interactable = false;
        rect.DOShakeAnchorPos(noTime, power, vib, 45);
        yield return noWait;
        moneyBtn.interactable = true;
        yield break;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoneyButtonPositionSetting : MonoBehaviour
{
    /// <summary>
    /// 내림차순으로 50000 - 10000 - 5000 - 1000
    /// </summary>
    [Header("각 버튼들")]
    public RectTransform[] buttons = new RectTransform[4];

    /// <summary>
    /// 내림차순으로 50000 - 10000 - 5000 - 1000
    /// </summary>
    [Header("각 위치")]
    public Vector2[] btnPosition = new Vector2[4];

    [Header("메뉴 세팅 버튼 텍스트")]
    public Text btnSettingText;

    private string[] btnTexts = new string[2];

    /// <summary>
    /// 50000원 기준 왼쪽 오른쪽
    /// </summary>
    public enum Position
    {
        LEFT, RIGHT
    }
    private Position pos;

    private void Awake()
    {
        pos = GetPosition(PlayerPrefs.GetInt("ButtonPosition", 0));
    }

    private void Start()
    {
        SetButtonPos();
        btnTexts[0] = "50000 10000\n5000 1000";
        btnTexts[0] = "10000 50000\n1000 5000";

    }

    public void BtnSetting()
    {
        switch(pos)
        {
            case Position.LEFT:
                pos = GetPosition(1);
                PlayerPrefs.SetInt("ButtonPosition", 1);
                break;
            case Position.RIGHT:
                pos = GetPosition(0);
                PlayerPrefs.SetInt("ButtonPosition", 0);
                break;
        }
        SetButtonPos();
    }

    private void SetButtonPos()
    {
        switch(pos)
        {
            case Position.LEFT:
                for (int i = 0; i < buttons.Length; i++)
                    buttons[i].anchoredPosition = btnPosition[i];
                btnSettingText.text = "50000 10000\n5000 1000";
                break;
            case Position.RIGHT:
                buttons[0].anchoredPosition = btnPosition[1];
                buttons[1].anchoredPosition = btnPosition[0];
                buttons[2].anchoredPosition = btnPosition[3];
                buttons[3].anchoredPosition = btnPosition[2];
                btnSettingText.text = "10000 50000\n1000 5000";
                break;
        }
    }

    private Position GetPosition(int index)
    {
        switch(index)
        {
            case 0:
                return Position.LEFT;
            default:
                return Position.RIGHT;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EffectSoundSetting : MonoBehaviour
{
    [Header("버튼 텍스트 이건 추후 그림으로 바꿈")]
    public Text btnText;

    [Header("효과음을 재생하는지 안하는지")]
    private bool playSound;
    public bool PlaySound { get => playSound; set => playSound = value; }

    private void Awake()
    {
        int play = PlayerPrefs.GetInt("EffectSound", 1);

        if (play == 0) PlaySound = false;
        else PlaySound = true;
        VisualSetting();
    }

    public void BtnTouch()
    {
        if (PlaySound) PlayerPrefs.SetInt("EffectSound", 0);
        else PlayerPrefs.SetInt("EffectSound", 1);

        PlaySound = !PlaySound;
        VisualSetting();
    }

    private void VisualSetting()
    {
        if (PlaySound) btnText.text = string.Format("Effect\nSound\nO");
        else btnText.text = string.Format("Effect\nSound\nX");
    }
}
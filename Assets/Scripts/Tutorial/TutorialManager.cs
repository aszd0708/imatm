﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * 처음에 단계는 무조건 1
 * 타이머는 스탑 해놓고 원할때 스타트 하기
 * 
 */

public class TutorialManager : MonoBehaviour
{
    [Header("취소 버튼이 나오는 시간")]
    public float disableTime;

    [Header("튜토리얼 패널")]
    public RectTransform tutorialPanel;

    [Header("패널 끄는 버튼")]
    public GameObject tutorialPanelExitBtn;

    [Header("세팅 끄는 버튼")]
    public Button settingPanelExitBtn;

    private void Start()
    {
        bool isFirstGaming = IntToBool.GetBool(PlayerPrefs.GetInt("FirstGaming", 1));
        if (isFirstGaming)
            ShowTutorialPanel();
        else return;
    }

    public void ShowTutorialPanel()
    {
        StartCoroutine(_ShowTutorialPanel());
    }

    private IEnumerator _ShowTutorialPanel()
    {
        tutorialPanelExitBtn.SetActive(false);
        PopupManager.Instance.OpenPopup(tutorialPanel, false);
        SetSettingPanelExitBtn(false);
        yield return new WaitForSeconds(2.0f);
        tutorialPanelExitBtn.SetActive(true);
        yield break;
    }

    public void SetSettingPanelExitBtn(bool isEnable)
    {
        settingPanelExitBtn.enabled = isEnable;
    }
}

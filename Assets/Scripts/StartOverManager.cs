﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * 2020-05-18
 * 게임이 실행되고 끝나는 스크립트
 *
 * 게임이 실행되면 돈 버튼생기고 타이머 시작
 * 게임이 오버 되면 돈 버튼이 없어지고 게임오버 나오고 타이머 멈춤
 */

public class StartOverManager : MonoBehaviour
{
    [Header("돈 버튼들 판넬")]
    public GameObject moneyBtnPanel;

    [Header("게임 오버 판넬")]
    public GameObject gameOverPanel;

    [Header("타임 매니저")]
    private TimeManager timeManager;

    [Header("스코어 매니저")]
    private ScoreManager scoreManager;

    [Header("돈 주는 컨트롤러")]
    private PlayerGiveMoneyController giveMoneyController;

    [Header("생명력 컨트롤러")]
    private PlayerLifeController playerLifeController;

    [Header("플레이어 컨트롤러")]
    private PlayerController playerController;

    [Header("웨이브 컨트롤러")]
    private WaveLevelController waveController;

    [Header("사람 줄 컨트롤러")]
    public PeopleLineController lineController;

    [Header("돈 카운트 컨트롤러")]
    public UserDataSaveLoad userData;

    [Header("사람들 생성기")]
    public PeopleCreater creater;

    [Header("Vip사람 컨트롤러")]
    public VIPPersonController vipController;

    [Header("게임 문 오픈! 클로즈")]
    public GameObject door;

    [Header("스타트 카드")]
    public StartButtonCardAnimation startCard;

    [Header("처음 콜렉트한거 컨트로로로로로ㅗ롤")]
    public VIPPersonFirstCollect firstCollectController;

    [Header("VIP List 컨트롤러")]
    public VIPPersonScorllListCreater vipListController;

    [Header("BGM")]
    public AudioSource BGM;

    [Header("NewMark커트롤")]
    public VIPFirstCollectImgController newMark;

    [Header("메뉴 버튼들")]
    public GameObject[] menuBtns;

    [Header("VIP Infor Loader")]
    public VIPPersonInfoLoader VIPInfo;

    private bool collectVIP;
    public bool CollectVIP { get => collectVIP; set => collectVIP = value; }

    private void Awake()
    {
        timeManager = GetComponent<TimeManager>();
        scoreManager = GetComponent<ScoreManager>();
        waveController = GetComponent<WaveLevelController>();

        giveMoneyController = FindObjectOfType<PlayerGiveMoneyController>();
        playerLifeController = FindObjectOfType<PlayerLifeController>();
        playerController = FindObjectOfType<PlayerController>();
    }

    private void Start()
    {
        GPGSSubject.Instance.Notify();
    }

    public void GameStart()
    {
        GameManager.Instance.IsGameOver = false;
        CollectVIP = false;
        StartCoroutine(_GameStart());
    }

    private IEnumerator _GameStart()
    {
        GameManager.Instance.IsGameOver = false;
        AudioManager.Instance.PlaySound(AudioManager.AudioKinds.START,Vector3.zero);

        for (int i = 0; i < menuBtns.Length; i++)
            menuBtns[i].SetActive(false);

        vipController.BonusSetting();
        door.GetComponent<IDoorAnimation>().OpenAnimation();

        startCard.Animation(true);
        lineController.SendMessage("StartSetting", SendMessageOptions.DontRequireReceiver);
        waveController.SendMessage("BasicSetting", SendMessageOptions.DontRequireReceiver);
        playerLifeController.SendMessage("StartSetting", SendMessageOptions.DontRequireReceiver);
        scoreManager.SendMessage("StartScore", SendMessageOptions.DontRequireReceiver);
        //waveController.PeopleCount = 0;
        timeManager.PeopleCount = 0;
        SetPapers();

        if(vipController.FirstBonus)
            scoreManager.StartScore(VIPInfo.PeopleInfo[userData.UserData.SelectFirstVipPerson].OpenScoreInt);

        yield return new WaitForSeconds(1.0f);
        BGM.Play();
        if (!vipController.FirstBonus)
        {
            timeManager.SendMessage("TimerStart", SendMessageOptions.DontRequireReceiver);
            moneyBtnPanel.SetActive(true);
        }

        yield break;
    }

    private IEnumerator _TutorialStart()
    {
        GameManager.Instance.IsGameOver = false;
        AudioManager.Instance.PlaySound(AudioManager.AudioKinds.START, Vector3.zero);

        for (int i = 0; i < menuBtns.Length; i++)
            menuBtns[i].SetActive(false);

        vipController.BonusSetting();
        door.GetComponent<IDoorAnimation>().OpenAnimation();

        startCard.Animation(true);
        lineController.SendMessage("StartSetting", SendMessageOptions.DontRequireReceiver);
        waveController.SendMessage("BasicSetting", SendMessageOptions.DontRequireReceiver);
        playerLifeController.SendMessage("StartSetting", SendMessageOptions.DontRequireReceiver);
        scoreManager.SendMessage("StartScore", SendMessageOptions.DontRequireReceiver);

        timeManager.PeopleCount = 0;
        SetPapers();

        if (vipController.FirstBonus)
            scoreManager.StartScore(VIPInfo.PeopleInfo[userData.UserData.SelectFirstVipPerson].OpenScoreInt);

        yield return new WaitForSeconds(1.0f);
        BGM.Play();
        if (!vipController.FirstBonus)
        {
            timeManager.SendMessage("TimerStart", SendMessageOptions.DontRequireReceiver);
            moneyBtnPanel.SetActive(true);
        }

        yield break;
    }

    private void SetPapers()
    {
        int[] counts =
        {
            userData.UserData.Money50000Count,
            userData.UserData.Money10000Count,
            userData.UserData.Money5000Count,
            userData.UserData.Money1000Count
        };

        MoneyButtonController.Instance.SetPapers(counts);
    }

    private void GameOver()
    {
        GameManager.Instance.IsGameOver = true;
        StartCoroutine(_GameOver());
    }

    public void VipCollectGameOver()
    {
        //Debug.Log("VIP 콜렉트 처음 했음");
        StartCoroutine(_VipCollectGameOver());
        CollectVIP = false;
        vipListController.ClickBtn(userData.UserData.VipPersonCollect-1);
        // 이곳임
        newMark.SetNewMark(true);
        //vipListController.list[userData.UserData.VipPersonCollect-1].GetComponent<IVipScrollList>().FirstCollcet(true);
       // vipListController.list[userData.UserData.VipPersonCollect - 1].GetComponent<IVipScrollList>().IsCollect(true);
       // vipListController.list[userData.UserData.VipPersonCollect - 1].GetComponent<Button>().enabled = true;
        vipListController.OpenInfo();
    }

    private IEnumerator _GameOver()
    {
        BGM.Stop();
        door.GetComponent<IDoorAnimation>().ClosedAnimation();
        moneyBtnPanel.SetActive(false);

        scoreManager.SendMessage("ResetScore", SendMessageOptions.DontRequireReceiver);
        lineController.SendMessage("GameOverSetting", SendMessageOptions.DontRequireReceiver);
        timeManager.SendMessage("TimerStop", SendMessageOptions.DontRequireReceiver);
        giveMoneyController.SendMessage("ResetValues", SendMessageOptions.DontRequireReceiver);
        giveMoneyController.SendMessage("SaveUserData", SendMessageOptions.DontRequireReceiver);

        yield return new WaitForSeconds(0.5f);
        gameOverPanel.SetActive(true);
        yield return new WaitForSeconds(1.0f);
        //door.GetComponent<IDoorAnimation>().OpenAnimation();
        GPGSSubject.Instance.Notify();
        if (!CollectVIP)
        {
            yield return new WaitForSeconds(1.0f);
            startCard.Animation(false);
            gameOverPanel.SetActive(false);
        }
        else
        {
            firstCollectController.StartCollectPanel();
        }
        for (int i = 0; i < menuBtns.Length; i++)
            menuBtns[i].SetActive(true);
        yield break;
    }

    private IEnumerator _VipCollectGameOver()
    {
        yield return new WaitForSeconds(1.0f);
        startCard.Animation(false);
        gameOverPanel.SetActive(false);
        yield break;
    }
}
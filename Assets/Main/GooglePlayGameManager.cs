﻿using UnityEngine;
using UnityEngine.UI;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;
using UnityEngine.SceneManagement;

public class GooglePlayGameManager : MonoBehaviour
{
    [SerializeField] private Text txtLog;
    [SerializeField] private InputField inputScore;

    [Header("구글 플레이 버튼 세팅")]
    public GGPSBtnSetting GBSetting;

    [Header("구글 클라우드 데이터 바이트로 끌고옴")]
    public UserDataSaveLoad data;

    public Text LogText;

    private void Awake()
    {
        PlayGamesPlatform.InitializeInstance(new PlayGamesClientConfiguration.Builder().EnableSavedGames().Build());
        PlayGamesPlatform.DebugLogEnabled = true;
        PlayGamesPlatform.Activate();
        OnBtnLoginClicked();
    }

    public void OnBtnLoginClicked()
    {
        //이미 인증된 사용자는 바로 로그인 성공된다.
        if (Social.localUser.authenticated)
        {
            Debug.Log("로그인!!!!" + Social.localUser.userName);
            txtLog.text += "name : " + Social.localUser.userName + "\n";
            GBSetting.Login();
        }
        else
        {
            Social.localUser.Authenticate((bool success) =>
            {
                if (success)
                {
                    Debug.Log("로그인!!!!" + Social.localUser.userName);
                    GBSetting.Login();
                }
                else
                {
                    //Debug.Log("Login Fail");
                    GBSetting.Logout();
                }
            });
        }
    }

    public void OnBtnLogoutClicked()
    {
        if (PlayGamesPlatform.Instance.IsAuthenticated())
        {
            ((PlayGamesPlatform)Social.Active).SignOut();
            GBSetting.Logout();
            PlayGamesPlatform.Instance.SignOut();
            Debug.Log("로그아웃!!!!");
        }
        //PlayGamesPlatform.Instance.SignOut();
    }

    public void OnBtnQuitClicked()
    {
        Application.Quit();
    }

    public void OnBtnShowLeaderboardClicked()
    {
        //Social.ShowLeaderboardUI();
        //PlayGamesPlatform.Instance.ShowLeaderboardUI(GPGSIds.leaderboard_1, LeaderboardTimeSpan.AllTime, (UIStatus status) =>
        ((PlayGamesPlatform)Social.Active).ShowLeaderboardUI(GPGSIds.leaderboard, LeaderboardTimeSpan.AllTime, (UIStatus status) =>
        {
            //Debug.Log("status = " + status);
            txtLog.text += "status = " + status;
        });
    }

    public void OnBtnReportScoreClicked()
    {
        //PlayGamesPlatform.Instance.ReportScore(int.Parse(txtScore.text), GPGSIds.leaderboard_1, (bool success) =>)
        //Social.ReportScore(int.Parse(inputScore.text), GPGSIds.leaderboard_1, (bool success) =>
        ((PlayGamesPlatform)Social.Active).ReportScore(int.Parse(inputScore.text), GPGSIds.leaderboard, (bool success) =>
        {
            if (success)
            {
                //Debug.Log("Report Success");
                txtLog.text += "Report Success";
            }
            else
            {
                //Debug.Log("Report Fail");
                txtLog.text += "Report Fail";
            }
        });
    }

    /// <summary>
    /// 구글게임플레이 서비스 로그인 확인용
    /// </summary>
    /// <returns></returns>
    public bool GetLogin()
    {
        return ((PlayGamesPlatform)Social.Active).IsAuthenticated();
    }

    public string GetUserName()
    {
        return Social.localUser.userName;
    }

    public void OnBtnShowAchievementClick()
    {
        Social.ShowAchievementsUI();
    }

    public void GetAchievement(string name)
    {
        //Debug.Log(name + "도전과제 완료");
        //PlayGamesPlatform.Instance.IncrementAchievement(name, 1, null);
        Social.ReportProgress(name, 100.0f, null);
    }

    #region 클라우드 저장

    ISavedGameClient SavedGame()
    {
        return PlayGamesPlatform.Instance.SavedGame;
    }


    public void LoadCloud()
    {
        SavedGame().OpenWithAutomaticConflictResolution("imatmdata",
            DataSource.ReadCacheOrNetwork, ConflictResolutionStrategy.UseLastKnownGood, LoadGame);
    }

    void LoadGame(SavedGameRequestStatus status, ISavedGameMetadata game)
    {
        if (status == SavedGameRequestStatus.Success)
            SavedGame().ReadBinaryData(game, LoadData);
    }

    void LoadData(SavedGameRequestStatus status, byte[] LoadedData)
    {
        if (status == SavedGameRequestStatus.Success)
        {
            data.SetDataToByte(LoadedData);
            SceneManager.LoadScene("MainScene");
            LogText.text = "로드댐";
        }
        else LogText.text = "로드 실패";
    }

    public void SaveCloud()
    {
        SavedGame().OpenWithAutomaticConflictResolution("imatmdata",
            DataSource.ReadCacheOrNetwork, ConflictResolutionStrategy.UseLastKnownGood, SaveGame);
    }

    public void SaveGame(SavedGameRequestStatus status, ISavedGameMetadata game)
    {
        if (status == SavedGameRequestStatus.Success)
        {
            var update = new SavedGameMetadataUpdate.Builder().Build();
            byte[] bytes = data.GetDataToByte();
            SavedGame().CommitUpdate(game, update, bytes, SaveData);
        }
    }

    void SaveData(SavedGameRequestStatus status, ISavedGameMetadata game)
    {
        if (status == SavedGameRequestStatus.Success)
        {
            LogText.text = "저장 성공";
        }
        else LogText.text = "저장 실패";
    }



    public void DeleteCloud()
    {
        SavedGame().OpenWithAutomaticConflictResolution("imatmdata",
            DataSource.ReadCacheOrNetwork, ConflictResolutionStrategy.UseLongestPlaytime, DeleteGame);
    }

    void DeleteGame(SavedGameRequestStatus status, ISavedGameMetadata game)
    {
        if (status == SavedGameRequestStatus.Success)
        {
            SavedGame().Delete(game);
            LogText.text = "삭제 성공";
        }
        else LogText.text = "삭제 실패";
    }

    #endregion
}